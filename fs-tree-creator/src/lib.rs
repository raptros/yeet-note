//! # fs_tree_creator
//! `fs_tree_creator` is a utility to make it easy to specify and create a bunch of files and directories.
//! this is useful for testing libraries and apps that interact heavily with directory structures.

use std::fs;
use std::io::Result;
use std::io::Write;
use std::path::Path;
use std::path::PathBuf;

#[cfg(test)]
mod tests;

/// represent the type of the entry at the path.
#[derive(Debug)]
pub enum FSEntryType {
    File(String),
    Directory(Vec<FSNode>),
    Symlink(PathBuf),
}

impl FSEntryType {
    /// construct a new file entry type with specified contents
    pub fn new_file(contents: String) -> Self {
        FSEntryType::File(contents)
    }

    /// construct a new file entry type with specified child nodes
    pub fn new_dir(children: Vec<FSNode>) -> Self {
        FSEntryType::Directory(children)
    }

    /// construct a symlink pointing to the other path
    pub fn new_symlink<P: AsRef<Path>>(pointing_to: P) -> Self {
        FSEntryType::Symlink(pointing_to.as_ref().to_path_buf())
    }
}

/// a node, or path, to be created in the filesystem
#[derive(Debug)]
pub struct FSNode {
    name: String,
    entry_type: FSEntryType,
}

impl FSNode {
    /// create an FS Node
    pub fn new(name: String, entry_type: FSEntryType) -> Self {
        FSNode { name, entry_type }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn entry_type(&self) -> &FSEntryType {
        &self.entry_type
    }
}

/// The root of an FS tree - the top level list of nodes to be created
#[derive(Debug)]
pub struct FSTree {
    entries: Vec<FSNode>,
}

impl FSTree {
    /// Create an FS tree from a list of entries to create under the directory.
    pub fn new(entries: Vec<FSNode>) -> Self {
        FSTree { entries }
    }

    /// Create the tree under the specified path.
    /// If any operation fails, this will fail immediately and return the error.
    pub fn create_at<P: AsRef<Path>>(self, base_path: P) -> Result<()> {
        FSTreeCreator::new(base_path).create(self)
    }

    /// Create a TempDir and build this tree within it.
    /// If any of the operations fail, this will fail immediately and return the error
    pub fn temp_dir(self) -> Result<tempfile::TempDir> {
        let temp_dir = tempfile::tempdir()?;
        self.create_at(temp_dir.path())?;
        Ok(temp_dir)
    }

    pub fn entries(&self) -> &Vec<FSNode> {
        &self.entries
    }
}

struct FSTreeCreator {
    path_buf: PathBuf,
}

impl FSTreeCreator {
    fn new<P: AsRef<Path>>(base_path: P) -> Self {
        FSTreeCreator {
            path_buf: base_path.as_ref().to_path_buf(),
        }
    }

    fn create(mut self, tree: FSTree) -> Result<()> {
        let entries = tree.entries;
        self.create_entries_here(entries)
    }

    fn create_entries_here(&mut self, entries: Vec<FSNode>) -> Result<()> {
        for entry in entries {
            self.create_node(entry)?;
        }
        Ok(())
    }

    fn create_node(&mut self, node: FSNode) -> Result<()> {
        self.path_buf.push(&node.name);
        self.create_here(node.entry_type)?;
        self.path_buf.pop();
        Ok(())
    }

    fn create_here(&mut self, entry_type: FSEntryType) -> Result<()> {
        match entry_type {
            FSEntryType::File(contents) => self.create_file_here(&contents),
            FSEntryType::Directory(children) => self.create_dir_here(children),
            FSEntryType::Symlink(pointing_to) => self.create_symlink_here(pointing_to),
        }
    }

    fn create_file_here(&self, contents: &str) -> Result<()> {
        let mut file = fs::File::create(&self.path_buf)?;
        write!(file, "{}", contents)
    }

    fn create_dir_here(&mut self, entries: Vec<FSNode>) -> Result<()> {
        //first, create a directory at the current path
        //note that this dies if the dir already exists!
        fs::create_dir(&self.path_buf)?;
        //then create the entries that belong in here.
        self.create_entries_here(entries)
    }

    fn create_symlink_here(&self, _pointing_to: PathBuf) -> Result<()> {
        //todo
        Ok(())
    }
}

/// Specify an entire FS tree that can then be created under a directory
///
/// # Examples
///
/// ```
/// # #[macro_use] extern crate fs_tree_creator;
/// # use fs_tree_creator::*;
/// # fn main() -> std::io::Result<()> {
/// let tree: FSTree = fs_tree![];
///
/// let temp_dir: tempfile::TempDir = tree.temp_dir()?;
/// # Ok(())
/// # }
/// ```
///
/// ```
/// # #[macro_use] extern crate fs_tree_creator;
/// # use fs_tree_creator::*;
/// # fn main() -> std::io::Result<()> {
/// let tree: FSTree = fs_tree![
///     file "test.txt" => "nothing";
/// ];
///
/// let temp_dir: tempfile::TempDir = tree.temp_dir()?;
/// # Ok(())
/// # }
/// ```
///
/// ```
/// # #[macro_use] extern crate fs_tree_creator;
/// # use fs_tree_creator::*;
/// # fn main() -> std::io::Result<()> {
/// let tree: FSTree = fs_tree![
///     file "test0.txt" => "nothing";
///     file "test1.txt" => "nothing";
/// ];
///
/// let temp_dir: tempfile::TempDir = tree.temp_dir()?;
/// # Ok(())
/// # }
/// ```
///
/// ```
/// # #[macro_use] extern crate fs_tree_creator;
/// # use fs_tree_creator::*;
/// # fn main() -> std::io::Result<()> {
/// let tree: FSTree = fs_tree![
///     dir "empty" => [
///     ];
/// ];
///
/// let temp_dir: tempfile::TempDir = tree.temp_dir()?;
/// # Ok(())
/// # }
/// ```
///
/// ```
/// # #[macro_use] extern crate fs_tree_creator;
/// # use fs_tree_creator::*;
/// # fn main() -> std::io::Result<()> {
/// let tree: FSTree = fs_tree![
///     dir "a" => [
///         file "test0.txt" => "nothing";
///         file "test1.txt" => "nothing";
///         dir "b" => [
///             file "test3.txt" => "";
///             dir "c" => [
///             ];
///             file "test4.txt" => "";
///         ];
///     ];
///     dir "d" => [
///         file "test5.txt" => "this is a bunch of text";
///     ];
/// ];
///
/// let temp_dir: tempfile::TempDir = tree.temp_dir()?;
/// # Ok(())
/// # }
///
#[macro_export]
macro_rules! fs_tree {
    (@e $mvec:ident,) => {};

    (@e $mvec:ident,file $n:expr => $c:expr; $($x:tt)*) => {
        $mvec.push($crate::FSNode::new(
            $n.to_string(),
            $crate::FSEntryType::new_file($c.to_string()),
        ));
        fs_tree![@e $mvec,$($x)*];
    };

    (@e $mvec:ident,dir $n:expr => [$($c:tt)*]; $($x:tt)*) => {
        let d = {
            #[allow(unused_mut)]
            let mut dvec = Vec::new();
            fs_tree![@e dvec,$($c)*];
            $crate::FSNode::new($n.to_string(), $crate::FSEntryType::new_dir(dvec))
        };
        $mvec.push(d);
        fs_tree![@e $mvec,$($x)*];
    };

    ($($x:tt)*) => {
        {
            #[allow(unused_mut)]
            let mut mvec = Vec::new();
            fs_tree![@e mvec, $($x)*];
            $crate::FSTree::new(mvec)
        }
    };
}
