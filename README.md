yeet-note
=========

simple note indexing tool

structure
---------

* yyyy/mm/dd/ as path to notes in a workspace
* workspace toplevel dir has .notes-workspace/
* notes program determines workspace by recursing up (like git)
* fancy vs simple notes - simple note is just a .md file, fancy note is a directory

tagging
-------

notes have tags. do tags have structure? could.

tool should be able to find notes by tags.

backend
-------

sqlite, ofc

architecture
------------

the major question is: do we have a separate agent process or not?



indexing backend
----------------

we do want to use sqlite as the backing index store
* we need a table representing the file system structure anyway
* we'll need to do the operation of deleting directory trees from it anyway
* better to only do it once

therefore the table's columns should be
* ID (auto-incrementing number, primary key)
* name (just the name)
* metadata fields (type, last update, size, etc)
* direct parent ID
* path - an array of all parents, starting at 0 (base directory)

base directory is therefore represented as
* ID 0
* name can be the full path string
* the last update field is the last time the cache was re-indexed
* direct parent ID is null
* "path" is empty

we can therefore easily determine if an index table is initialized by looking for ID 0.

indexing algo
-------------

iterate over directory scan events. maintain a stack.
* push stack item on "enter directory". fetch list of indexed entries.
* make sure that index and scanner return entries in same order 
  (this permits an optimization for determining created and deleted entries)
* can we get a future?

license
-------

yeet-note is licensed under the terms of the MIT License or the Apache License 2.0, at your choosing.