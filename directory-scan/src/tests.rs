use fs_tree_creator::*;

use crate::ScanWithIndex;
use failure::Fallible;
use tempfile::TempDir;

use crate::file_index::fake::*;

#[test]
fn single_file() -> Fallible<()> {
    let tempdir: TempDir = fs_tree![
        file "hello.txt" => "HELLO WORLD";
    ]
    .temp_dir()?;

    let fake_file_index = FakeFileIndex::new();

    let scan_with_index = ScanWithIndex::new(tempdir.path(), fake_file_index);

    Ok(())
}
