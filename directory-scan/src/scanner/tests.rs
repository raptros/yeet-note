use std::path::Path;

use failure::*;
use tempfile::TempDir;

use fs_tree_creator::*;

use crate::fs;
use crate::scanner::*;

macro_rules! expect_next {
    ($e: ident) => {
        $e.next().expect("missing an element")
    };
}

macro_rules! expect_entry {
    ($e: expr) => {
        match $e {
            DirScanEvent::LeaveDir => panic!("got LeaveDir instead of Entry"),
            DirScanEvent::EnterDir => panic!("got LeaveDir instead of Entry"),
            DirScanEvent::Entry(v) => v,
        }
    };
}

fn get_scan_results<P: AsRef<Path>>(base_path: P) -> DirScanResult<Vec<DirScanEvent>> {
    let scan = DirScan::new(base_path)
        .min_depth(1)
        .append_leave_dir(true)
        .sort_by(sort_names);

    scan.into_iter().collect()
}

#[test]
fn empty_base_dir() -> Fallible<()> {
    let temp_dir = TempDir::new()?;

    let base_path = temp_dir.path();

    let expected = vec![DirScanEvent::LeaveDir];

    let result = get_scan_results(base_path)?;

    assert_eq!(result, expected);

    Ok(())
}

#[test]
fn single_item_base_dir() -> Fallible<()> {
    let temp_dir: TempDir = fs_tree![
        file "sample.txt" => "";
    ]
    .temp_dir()?;

    let results = get_scan_results(temp_dir.path())?;

    assert_eq!(results.len(), 2);

    let mut r_iter = results.into_iter();

    let res: fs::Entry = expect_entry!(expect_next!(r_iter));

    assert_eq!(res.name(), "sample.txt");

    assert_eq!(expect_next!(r_iter), DirScanEvent::LeaveDir);

    Ok(())
}

#[test]
fn multi_layer_step_back() -> Fallible<()> {
    let temp_dir = fs_tree![
        dir "a" => [
            dir "b" => [
                file "deep.txt" => "";
            ];
        ];
        file "sample.txt" => "";
    ]
    .temp_dir()?;

    let results = get_scan_results(temp_dir.path())?;

    assert_eq!(results.len(), 9);

    let mut r_iter = results.into_iter();

    let res: fs::Entry = expect_entry!(expect_next!(r_iter));
    assert_eq!(res.name(), "a");
    assert_eq!(res.entry_type(), &fs::EntryType::Directory);

    assert_eq!(expect_next!(r_iter), DirScanEvent::EnterDir);

    let res: fs::Entry = expect_entry!(expect_next!(r_iter));
    assert_eq!(res.name(), "b");
    assert_eq!(res.entry_type(), &fs::EntryType::Directory);

    assert_eq!(expect_next!(r_iter), DirScanEvent::EnterDir);

    let res: fs::Entry = expect_entry!(expect_next!(r_iter));
    assert_eq!(res.name(), "deep.txt");
    assert_eq!(res.entry_type(), &fs::EntryType::File);

    //leaving b
    assert_eq!(expect_next!(r_iter), DirScanEvent::LeaveDir);

    //leaving a
    assert_eq!(expect_next!(r_iter), DirScanEvent::LeaveDir);

    let res: fs::Entry = expect_entry!(expect_next!(r_iter));
    assert_eq!(res.name(), "sample.txt");
    assert_eq!(res.entry_type(), &fs::EntryType::File);

    //leaving <tmp>
    assert_eq!(expect_next!(r_iter), DirScanEvent::LeaveDir);
    Ok(())
}
