-- Your SQL goes here
PRAGMA foreign_keys = 1;

-- a note - either a markdown file, or a directory containing notes
CREATE TABLE notes (
    id INTEGER PRIMARY KEY,

    -- foreign key for parent note id
    -- the parent note cannot be null because NULLs are treated as non-equal for unique indexes,
    -- and we need a unique index on (parent, name)
    -- this means there must be a self-referential root note.
    parent_note_id BIGINT NOT NULL,

    -- top level name for the note, if available (file name stem or directory name (or both))
    note_name TEXT NOT NULL,

    -- nullable note date - path should override file data
    -- todo: indexing on date
    -- todo: what do we do about say year level notes? i say just let them have specific dates as well.
    note_date DATE,

    --we don't keep the path b/c this note record could be both a file and a directory.

    --a ref to a directory for this note
    directory_file_id BIGINT,
    --a ref to a markdown file for this note
    md_file_id BIGINT,
    --in the future, other types of adjacent files could be recorded

    CONSTRAINT has_parent_note FOREIGN KEY (parent_note_id) REFERENCES notes (id) ON DELETE CASCADE,
    CONSTRAINT unique_parent_and_name UNIQUE (parent_note_id, note_name),

    -- directory file id is a foreign key and it is unique indexed
    CONSTRAINT can_point_to_directory FOREIGN KEY (directory_file_id) REFERENCES file_index (id) ON DELETE SET NULL,
    CONSTRAINT dir_to_single_note UNIQUE (directory_file_id),

    -- md file id is a foreign key and it is unique indexed
    CONSTRAINT can_point_to_md_file FOREIGN KEY (md_file_id) REFERENCES file_index (id) ON DELETE SET NULL,
    CONSTRAINT file_to_single_note UNIQUE (md_file_id)
);

CREATE TRIGGER note_removal
    AFTER UPDATE OF directory_file_id, md_file_id
    ON notes
    WHEN NEW.directory_file_id IS NULL AND NEW.md_file_id IS NULL
BEGIN
    DELETE FROM notes WHERE id=NEW.id;
END;

CREATE TRIGGER enforce_exists_in_fs
    BEFORE INSERT ON notes
BEGIN
    SELECT CASE WHEN NEW.directory_file_id IS NULL AND NEW.md_file_id IS NULL THEN
    -- note: if this message ever changes, then notes::tests::must_have_file_or_dir() must be updated as well
        RAISE (ABORT, 'note must be created with at least one file reference')
    END;
END;

INSERT INTO notes (
    id,
    parent_note_id,
    note_name,
    note_date,
    directory_file_id,
    md_file_id
) VALUES (
    1,
    1,
    "",
    NULL, -- no date.
    1, -- point to the root of the file index.
    NULL -- can't be an adjacent file to the root anyway!
);