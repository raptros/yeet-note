PRAGMA foreign_keys = 1;

CREATE TABLE file_index (
    id INTEGER PRIMARY KEY,
    parent_id BIGINT NOT NULL,
    name TEXT NOT NULL,
    depth BIGINT NOT NULL,
    md_modified BIGINT NOT NULL, -- use this as seconds since unix epoch
    md_size_bytes BIGINT NOT NULL,
    is_dir BOOLEAN NOT NULL DEFAULT 0,
    is_file BOOLEAN NOT NULL DEFAULT 0,
    is_symlink BOOLEAN NOT NULL DEFAULT 0,
    is_other BOOLEAN NOT NULL DEFAULT 0,
    first_index_time BIGINT NOT NULL, -- use this as seconds since unix epoch
    last_index_time BIGINT NOT NULL, -- use this as seconds since unix epoch

    CONSTRAINT has_parent FOREIGN KEY (parent_id) REFERENCES file_index (id) ON DELETE CASCADE,
    CONSTRAINT has_file_type CHECK ((is_dir + is_file + is_symlink + is_other) is 1)
);

CREATE INDEX file_index_parent_id ON file_index (parent_id);

-- create the root record
INSERT INTO file_index (
    id,
    parent_id,
    name,
    depth,
    md_modified,
    md_size_bytes,
    is_dir,
    first_index_time,
    last_index_time
) VALUES (
    1,
    1,
    "",
    0,
    strftime('%s', 'now'),
    0,
    1,
    strftime('%s', 'now'),
    strftime('%s', 'now')
);