PRAGMA foreign_keys = 1;

CREATE TABLE tags (
    id INTEGER PRIMARY KEY,
    -- i guess the app will have to perform normalization?
    -- it'd be nice if it could be done in sqlite
    tag_name TEXT NOT NULL,
    -- yes, tag names have to be unique.
    UNIQUE (tag_name)
);

-- join table between notes and tags
CREATE TABLE notes_to_tags (
    note_id BIGINT NOT NULL,
    tag_id BIGINT NOT NULL,

    PRIMARY KEY (note_id, tag_id),
    CONSTRAINT has_note FOREIGN KEY (note_id) REFERENCES notes (id) ON DELETE CASCADE,
    CONSTRAINT has_tag FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE
) WITHOUT ROWID;

-- this index does not need to be unique, as the primary key index already should enforce that.
CREATE INDEX tags_to_notes ON notes_to_tags (tag_id, note_id);
