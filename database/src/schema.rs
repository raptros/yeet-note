table! {
    file_index (id) {
        id -> BigInt,
        parent_id -> BigInt,
        name -> Text,
        depth -> BigInt,
        md_modified -> BigInt,
        md_size_bytes -> BigInt,
        is_dir -> Bool,
        is_file -> Bool,
        is_symlink -> Bool,
        is_other -> Bool,
        first_index_time -> BigInt,
        last_index_time -> BigInt,
    }
}

table! {
    notes (id) {
        id -> BigInt,
        parent_note_id -> BigInt,
        note_name -> Text,
        note_date -> Nullable<Date>,
        directory_file_id -> Nullable<BigInt>,
        md_file_id -> Nullable<BigInt>,
    }
}

table! {
    notes_to_tags (note_id, tag_id) {
        note_id -> BigInt,
        tag_id -> BigInt,
    }
}

table! {
    tags (id) {
        id -> BigInt,
        tag_name -> Text,
    }
}

joinable!(notes_to_tags -> notes (note_id));
joinable!(notes_to_tags -> tags (tag_id));

allow_tables_to_appear_in_same_query!(file_index, notes, notes_to_tags, tags,);
