use std::path::PathBuf;

use diesel::prelude::*;
use diesel::sql_query;
use diesel::sql_types::BigInt;

use directory_scan::file_index::FileIndex;
use directory_scan::fs;
use yn_utils::ResultConvert;

use crate::connect_result::DbQueryError;
use crate::connect_result::DbQueryResult;
use crate::file_index::models::AncestorRecord;
use crate::file_index::models::DBIndexEntry;
pub use crate::file_index::models::FileIndexKey;
use crate::file_index::models::NewDBIndexEntry;
use crate::file_index::models::UpdateDbIndexEntry;
use crate::Db;
use std::time::SystemTime;

pub mod models;

#[cfg(test)]
pub(crate) mod tests;

pub struct FileIndexTable<'a> {
    pub db: &'a Db,
}

impl<'a> FileIndexTable<'a> {
    pub fn insert(&self, p_id: FileIndexKey, entry: &fs::Entry) -> DbQueryResult<FileIndexKey> {
        use crate::schema::file_index::dsl::*;
        let new_entry = NewDBIndexEntry::new_from_entry(p_id, &entry);

        self.db.transaction::<FileIndexKey, DbQueryError, _>(|| {
            diesel::insert_into(file_index)
                .values(new_entry)
                .execute(self.db.conn())?;
            file_index
                .select(id)
                .order(id.desc())
                .first(self.db.conn())
                .from_err()
        })
    }

    pub fn get_by_id(&self, e_id: FileIndexKey) -> DbQueryResult<DBIndexEntry> {
        use crate::schema::file_index::dsl::*;
        file_index.find(e_id).first(&*self.db.conn()).from_err()
    }

    pub fn get_children(&self, e_id: FileIndexKey) -> DbQueryResult<Vec<DBIndexEntry>> {
        use crate::schema::file_index::dsl::*;
        file_index
            .filter(parent_id.eq(e_id).and(id.ne(e_id)))
            .order(id.asc())
            .load(self.db.conn())
            .from_err()
    }

    pub fn do_modify(&self, changes: &UpdateDbIndexEntry) -> DbQueryResult<()> {
        diesel::update(changes)
            .set(changes)
            .execute(self.db.conn())
            .void_res()
            .from_err()
    }

    pub fn modify(&self, e_id: FileIndexKey, entry: &fs::Entry) -> DbQueryResult<()> {
        self.do_modify(&UpdateDbIndexEntry::from_entry(e_id, entry))
    }

    pub fn update_root_time(&self, update_md_modified: bool) -> DbQueryResult<()> {
        use crate::schema::file_index::dsl::*;
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect("current time is before unix epoch time.")
            .as_secs() as i64;

        let expr = if update_md_modified {
            (last_index_time.eq(now), Some(md_modified.eq(now)))
        } else {
            (last_index_time.eq(now), None)
        };

        diesel::update(file_index.find(FileIndexKey::ROOT))
            .set(expr)
            .execute(self.db.conn())
            .void_res()
            .from_err()
    }

    pub fn remove(&self, e_id: FileIndexKey) -> DbQueryResult<()> {
        use crate::schema::file_index::dsl::*;
        diesel::delete(file_index.find(e_id))
            .execute(self.db.conn())
            .void_res()
            .from_err()
    }

    pub fn get_ancestors(&self, e_id: FileIndexKey) -> DbQueryResult<Vec<AncestorRecord>> {
        sql_query(include_str!("rec_ancestors.sql"))
            .bind::<BigInt, FileIndexKey>(e_id)
            .load::<AncestorRecord>(self.db.conn())
            .from_err()
    }

    pub fn get_path(&self, e_id: FileIndexKey) -> DbQueryResult<PathBuf> {
        self.get_ancestors(e_id)
            .map(|recs| itertools::rev(recs).map(|r| r.name).collect())
    }
}

impl<'a> FileIndex for FileIndexTable<'a> {
    type ID = FileIndexKey;
    type Entry = DBIndexEntry;
    //todo: error stuff
    type Error = DbQueryError;

    fn get_base(&self) -> Result<Self::Entry, Self::Error> {
        self.get_by_id(FileIndexKey::ROOT)
    }

    fn update_index_time(&self, updates_occurred: bool) -> Result<(), Self::Error> {
        self.update_root_time(updates_occurred)
    }

    fn list_dir(&self, id: Self::ID) -> Result<Vec<Self::Entry>, Self::Error> {
        self.get_children(id)
    }

    fn delete(&self, id: Self::ID) -> Result<(), Self::Error> {
        self.remove(id)
    }

    fn create(&self, parent_id: Self::ID, entry: &fs::Entry) -> Result<Self::ID, Self::Error> {
        self.insert(parent_id, entry)
    }

    fn update(&self, id: Self::ID, entry: &fs::Entry) -> Result<(), Self::Error> {
        self.modify(id, entry)
    }

    fn get_path(&self, id: Self::ID) -> Result<PathBuf, Self::Error> {
        self.get_path(id)
    }
}
