use std::path::PathBuf;
use std::time::Duration;
use std::time::SystemTime;

use failure::Fail;

use directory_scan::fs;

use crate::connect_result::DbQueryErrorKind;
use crate::tests::get_mem_db;

use super::models::UpdateDbIndexEntry;
use super::FileIndexKey;
use super::FileIndexTable;

pub(crate) fn fake_metadata() -> fs::Metadata {
    fs::Metadata::new(SystemTime::now(), 0)
}

fn check_time(time: i64, name: &str) {
    let t_time = SystemTime::UNIX_EPOCH + Duration::from_secs(time as u64);
    let cur_time = SystemTime::now();
    assert!(
        t_time <= cur_time,
        "{} is {:?}, cur_time is {:?}",
        name,
        t_time,
        cur_time
    );
    let earlier_time = cur_time - Duration::from_secs(20);
    assert!(
        t_time > earlier_time,
        "{} is {:?}, earlier_time is {:?}",
        name,
        t_time,
        earlier_time
    );
}

fn check_root(table: &FileIndexTable) {
    let root = table
        .get_by_id(FileIndexKey::ROOT)
        .expect("should have been able to get root entry");

    assert_eq!(root.id, FileIndexKey::ROOT);
    assert_eq!(root.parent_id, FileIndexKey::ROOT);
    assert_eq!(&root.name, "");

    check_time(root.md_modified, "md_modified");
    check_time(root.first_index_time, "first_index_time");
    check_time(root.last_index_time, "last_index_time");
}

#[test]
fn can_update_root_time() {
    let db = get_mem_db();
    let table = db.file_index();

    check_root(&table);

    let root_before = table
        .get_by_id(FileIndexKey::ROOT)
        .expect("should have been able to get root entry");

    std::thread::sleep(std::time::Duration::from_secs(1));

    table
        .update_root_time(false)
        .expect("update root time failed");

    let root_first_mod = table
        .get_by_id(FileIndexKey::ROOT)
        .expect("should have been able to get root entry");

    assert_eq!(root_before.md_modified, root_first_mod.md_modified);

    assert!(root_first_mod.last_index_time > root_before.last_index_time);

    std::thread::sleep(std::time::Duration::from_secs(1));

    table
        .update_root_time(true)
        .expect("update root time failed");

    let root_second_mod = table
        .get_by_id(FileIndexKey::ROOT)
        .expect("should have been able to get root entry");

    assert!(root_second_mod.md_modified > root_first_mod.md_modified);

    assert!(root_second_mod.last_index_time > root_first_mod.last_index_time);
}

#[test]
fn create_and_find() {
    let db = get_mem_db();
    let table = db.file_index();

    check_root(&table);

    let next_entry = fs::Entry::new(
        PathBuf::from("next1"),
        1,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let _root_child_1 = table
        .insert(FileIndexKey::ROOT, &next_entry)
        .expect("insert should have succeed");

    let entry2 = fs::Entry::new(
        PathBuf::from("next2"),
        1,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let root_child_2 = table
        .insert(FileIndexKey::ROOT, &entry2)
        .expect("insert should have succeed");

    let root_children = table
        .get_children(FileIndexKey::ROOT)
        .expect("get_children should have succeeded");

    assert_eq!(root_children.len(), 2, "got entries {:?}", root_children);

    assert_eq!(root_children[0].name, "next1");
    assert_eq!(root_children[1].name, "next2");

    let child2 = table
        .get_by_id(root_child_2)
        .expect("failed to get child 2");

    assert_eq!(child2.name, "next2");
}

#[test]
fn delete_cascades() {
    let db = get_mem_db();
    let table = db.file_index();

    check_root(&table);

    let next_entry = fs::Entry::new(
        PathBuf::from("next1"),
        1,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let root_child_1 = table
        .insert(FileIndexKey::ROOT, &next_entry)
        .expect("insert should have succeed");

    let entry2 = fs::Entry::new(
        PathBuf::from("next2"),
        2,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let child_2 = table
        .insert(root_child_1, &entry2)
        .expect("insert should have succeed");

    table
        .remove(FileIndexKey::ROOT)
        .expect("failed to delete root");

    let post_del_res = table
        .get_by_id(child_2)
        .expect_err("should not have been able to find child_2 after deleting root");

    assert_eq!(post_del_res.kind(), DbQueryErrorKind::QueryError);

    let cause = post_del_res
        .cause()
        .expect("there should have been a cause!");
    if let Some(diesel::result::Error::NotFound) = cause.downcast_ref::<diesel::result::Error>() {
    } else {
        panic!("got an unexpected error from the database: {:?}", cause)
    }
}

#[test]
fn can_get_path() {
    /* setup steps */
    let db = get_mem_db();
    let table = db.file_index();

    check_root(&table);

    let next_entry = fs::Entry::new(
        PathBuf::from("next1"),
        1,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let root_child_1 = table
        .insert(FileIndexKey::ROOT, &next_entry)
        .expect("insert should have succeed");

    let entry2 = fs::Entry::new(
        PathBuf::from("next2"),
        2,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let child_2 = table
        .insert(root_child_1, &entry2)
        .expect("insert should have succeed");

    let entry3 = fs::Entry::new(
        PathBuf::from("next3"),
        3,
        fs::EntryType::File,
        fake_metadata(),
    );

    let child_3 = table
        .insert(child_2, &entry3)
        .expect("insert should have succeed");

    /* first: test of getting ancestors */

    let ancestors = table
        .get_ancestors(child_3)
        .expect("error in get_ancestors call");

    assert_eq!(ancestors.len(), 3);

    assert_eq!(ancestors[0].name, "next3");
    assert_eq!(ancestors[1].name, "next2");
    assert_eq!(ancestors[2].name, "next1");

    /* second: test the path getting version */

    let path = table.get_path(child_3).expect("error in get_path");

    assert_eq!(path, PathBuf::from("next1/next2/next3"));
}

#[test]
fn cannot_insert_with_invalid_parent() {
    let db = get_mem_db();
    let table = db.file_index();

    check_root(&table);

    let bad_entry = fs::Entry::new(
        PathBuf::from("fake"),
        1,
        fs::EntryType::Directory,
        fake_metadata(),
    );

    let res = table.insert(FileIndexKey::wrap(3443), &bad_entry);

    println!("{:?}", res);

    let err = res.expect_err("insert with bad parent succeded when it should have failed");

    assert_eq!(err.kind(), DbQueryErrorKind::QueryError);

    let cause = err.cause().expect("there should have been a cause!");

    if let Some(diesel::result::Error::DatabaseError(kind, _msg)) =
        cause.downcast_ref::<diesel::result::Error>()
    {
        match kind {
            diesel::result::DatabaseErrorKind::ForeignKeyViolation => (),
            _ => panic!("got some other kind of database error: {:?}", kind),
        }
    } else {
        panic!("got an unexpected error from the database: {:?}", cause)
    }
}

#[test]
fn create_and_update() {
    let db = get_mem_db();
    let table = db.file_index();

    check_root(&table);

    let next_entry = fs::Entry::new(
        PathBuf::from("next.txt"),
        1,
        fs::EntryType::File,
        fake_metadata(),
    );

    let root_child_id = table
        .insert(FileIndexKey::ROOT, &next_entry)
        .expect("insert should have succeed");

    let new_size: i64 = 55555;

    let modified_metadata = fs::Metadata::new(SystemTime::now(), new_size);

    let updated =
        UpdateDbIndexEntry::from_parts(root_child_id, &modified_metadata, &fs::EntryType::Symlink);

    table.do_modify(&updated).expect("do_modify failed!");

    let updated_child = table
        .get_by_id(root_child_id)
        .expect("should have been able to retrieve updated_child!");

    assert_eq!(updated_child.id, root_child_id);

    assert_eq!(updated_child.md_size_bytes, new_size);

    assert_eq!(updated_child.is_file, false);
    assert_eq!(updated_child.is_symlink, true);
}
