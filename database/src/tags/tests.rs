use std::collections::HashSet;

use diesel::result::DatabaseErrorKind;
use diesel::result::Error as DieselError;
use failure::bail;
use failure::Fail;
use failure::Fallible;
use failure::ResultExt;

use yn_utils::ResultConvert;

use crate::file_index::FileIndexKey;
use crate::notes::model::NoteEntry;
use crate::notes::tests::create_markdown_file;
use crate::notes::NoteEntryType;
use crate::notes::NoteFileData;
use crate::tests::get_mem_db;
use crate::Db;

use super::model::TagEntry;
use super::TagName;

macro_rules! tag_set {
    ($($s:expr),*) => {
        vec![$(TagName::from($s),)*].into_iter().collect::<HashSet<_>>();
    };
}

pub(crate) fn make_test_note(db: &Db, name: &str) -> Fallible<NoteEntry> {
    let fname = name.to_owned() + ".md";
    let md_file_entry = create_markdown_file(db, &fname, FileIndexKey::ROOT)
        .context("couldn't create md file entry for test")?;

    let file_note_data = NoteFileData {
        name: name.to_owned(),
        date: None,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: md_file_entry,
        entry_type: NoteEntryType::Markdown,
    };

    let new_note_id = db
        .note_ops()
        .add_note_for_file(file_note_data)
        .context("failed to insert new note!")?;

    db.note_ops()
        .get_note_by_id(new_note_id)
        .context("what happened to the note that was created?")
        .from_err()
}

#[test]
fn create_some_tags() -> Fallible<()> {
    let db = get_mem_db();

    let tag_ops = db.tag_ops();

    let x = tag_set!["one", "two", "three"];
    let created = tag_ops
        .create(&x)?
        .into_iter()
        .map(|e| e.tag_name)
        .collect::<HashSet<_>>();

    assert_eq!(created, x);

    let found = tag_ops
        .query_names(&x)?
        .into_iter()
        .map(|e| e.tag_name)
        .collect::<HashSet<_>>();

    assert_eq!(found, created);

    Ok(())
}

#[test]
fn add_tags_to_note() -> Fallible<()> {
    let db = get_mem_db();

    let tag_ops = db.tag_ops();

    let x = tag_set!["one", "two", "three"];
    let mut created = tag_ops.create(&x)?;
    created.sort_unstable_by_key(TagEntry::id);

    let note_entry = make_test_note(&db, "test0")?;

    let created_ids = created.iter().map(TagEntry::id).collect::<Vec<_>>();

    tag_ops.add_to(note_entry.id, &created_ids)?;

    let mut note_tags = tag_ops.get_tags_for_note(note_entry.id)?;
    note_tags.sort_unstable_by_key(TagEntry::id);

    assert_eq!(note_tags, created);

    Ok(())
}

#[test]
fn remove_tag_from_note() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();

    let x = tag_set!["one"];
    let created = tag_ops.create(&x)?;
    assert_eq!(created.len(), 1);

    let note_entry = make_test_note(&db, "test0")?;

    let created_ids = created.iter().map(TagEntry::id).collect::<Vec<_>>();

    tag_ops.add_to(note_entry.id, &created_ids)?;

    let note_tags = tag_ops.get_tags_for_note(note_entry.id)?;
    assert_eq!(note_tags.len(), 1);

    tag_ops.remove_from(note_entry.id, &created_ids)?;

    let note_tags = tag_ops.get_tags_for_note(note_entry.id)?;
    assert_eq!(note_tags.len(), 0);

    Ok(())
}

#[test]
fn tag_names_are_unique() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();

    let x = tag_set!["one"];

    let created = tag_ops.create(&x)?;
    assert_eq!(created.len(), 1);

    let res = tag_ops
        .create(&x)
        .expect_err("second create should have failed!");

    let cause = res
        .cause()
        .expect("cause should be present")
        .downcast_ref::<DieselError>()
        .expect("cause should be a diesel error");

    match cause {
        DieselError::DatabaseError(DatabaseErrorKind::UniqueViolation, _) => (),
        _ => bail!("got some other kind of diesel error: {:?}", cause),
    }
    Ok(())
}

#[test]
fn tag_relations_are_unique() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();

    let x = tag_set!["one"];

    let created = tag_ops.create(&x)?;
    assert_eq!(created.len(), 1);

    let note_entry = make_test_note(&db, "test0")?;
    let created_ids = created.iter().map(TagEntry::id).collect::<Vec<_>>();
    tag_ops.add_to(note_entry.id, &created_ids)?;

    let add2_res = tag_ops
        .add_to(note_entry.id, &created_ids)
        .expect_err("second call to add_to should have failed");

    let cause = add2_res
        .cause()
        .expect("cause should be present")
        .downcast_ref::<DieselError>()
        .expect("cause should be a diesel error");

    match cause {
        DieselError::DatabaseError(DatabaseErrorKind::UniqueViolation, _) => (),
        _ => bail!("got some other kind of diesel error: {:?}", cause),
    }

    Ok(())
}

#[test]
fn delete_note_deletes_tag_rel_but_not_tag() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();

    let x = tag_set!["one"];

    let created = tag_ops.create(&x)?;
    assert_eq!(created.len(), 1);

    let note_entry = make_test_note(&db, "test0")?;
    let created_ids = created.iter().map(TagEntry::id).collect::<Vec<_>>();
    tag_ops.add_to(note_entry.id, &created_ids)?;

    let note_tags = tag_ops.get_tags_for_note(note_entry.id)?;
    assert_eq!(note_tags.len(), 1);

    db.file_index().remove(
        note_entry
            .md_file_id
            .expect("note entry should have md_file_id"),
    )?;

    assert!(db.note_ops().try_get_note_by_id(note_entry.id)?.is_none());

    let entry = tag_ops
        .try_by_id(created[0].id)?
        .expect("tag should still exist!");

    assert_eq!(entry, created[0]);

    Ok(())
}

#[test]
fn can_get_notes_by_tag() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();

    let x = tag_set!["one", "two", "three"];

    let mut created = tag_ops.create(&x)?;
    assert_eq!(created.len(), 3);
    created.sort_unstable_by_key(TagEntry::id);

    let note_entry_0 = make_test_note(&db, "test0")?;
    let note_entry_1 = make_test_note(&db, "test1")?;

    tag_ops.add_to(note_entry_0.id, &vec![created[0].id, created[1].id])?;

    tag_ops.add_to(note_entry_1.id, &vec![created[1].id, created[2].id])?;

    let tag_0_notes = tag_ops.get_notes_for_tag(created[0].id)?;

    assert_eq!(tag_0_notes.len(), 1);
    assert_eq!(tag_0_notes[0].id, note_entry_0.id);

    let mut tag_1_notes = tag_ops.get_notes_for_tag(created[1].id)?;

    assert_eq!(tag_1_notes.len(), 2);
    tag_1_notes.sort_unstable_by_key(|e| e.id);

    assert_eq!(tag_1_notes[0].id, note_entry_0.id);
    assert_eq!(tag_1_notes[1].id, note_entry_1.id);

    let tag_2_notes = tag_ops.get_notes_for_tag(created[2].id)?;
    assert_eq!(tag_2_notes.len(), 1);
    assert_eq!(tag_2_notes[0].id, note_entry_1.id);

    Ok(())
}

#[test]
fn ensure_exist_empty() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();

    let res = tag_ops.ensure_exist(HashSet::new())?;

    assert_eq!(res.created.len(), 0);
    assert_eq!(res.existed.len(), 0);

    Ok(())
}

#[test]
fn ensure_exist_one_new_tag() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();
    let x = tag_set!["new"];

    let name = TagName::from("new");

    let res = tag_ops.ensure_exist(x.clone())?;

    assert_eq!(res.created.len(), 1);
    assert_eq!(res.created[0].tag_name, name);
    assert_eq!(res.existed.len(), 0);

    Ok(())
}

#[test]
fn ensure_exist_one_new_tag_then_same_tag() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();
    let x = tag_set!["new"];
    let name = TagName::from("new");

    let res = tag_ops.ensure_exist(x.clone())?;

    assert_eq!(res.created.len(), 1);
    assert_eq!(res.created[0].tag_name, name);
    assert_eq!(res.existed.len(), 0);

    let res = tag_ops.ensure_exist(x.clone())?;

    assert_eq!(res.created.len(), 0);
    assert_eq!(res.existed.len(), 1);
    assert_eq!(res.existed[0].tag_name, name);

    Ok(())
}

#[test]
fn ensure_exist_several_tags() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();
    let x0 = tag_set!["one", "two", "three", "four"];

    let res = tag_ops.ensure_exist(x0.clone())?;

    assert_eq!(res.created.len(), 4);
    let created_names = res
        .created
        .into_iter()
        .map(|e| e.tag_name)
        .collect::<HashSet<_>>();
    assert_eq!(created_names, x0);
    assert_eq!(res.existed.len(), 0);

    let x1 = tag_set!["three", "four", "five", "six"];
    let res = tag_ops.ensure_exist(x1.clone())?;

    let created = res
        .created
        .into_iter()
        .map(|e| e.tag_name)
        .collect::<HashSet<_>>();

    let existed = res
        .existed
        .into_iter()
        .map(|e| e.tag_name)
        .collect::<HashSet<_>>();

    assert_eq!(created, tag_set!("five", "six"));

    assert_eq!(existed, tag_set!("three", "four"));

    let x2 = tag_set!["one", "three", "five"];
    let res = tag_ops.ensure_exist(x2.clone())?;

    let created = res.created;
    assert_eq!(created.len(), 0);

    let existed = res
        .existed
        .into_iter()
        .map(|e| e.tag_name)
        .collect::<HashSet<_>>();

    assert_eq!(existed, x2);

    Ok(())
}

#[test]
fn set_tags_clear() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();

    let x = tag_set!["one", "two", "three"];
    let mut created = tag_ops.create(&x)?;
    created.sort_unstable_by_key(TagEntry::id);
    let created_ids = created.iter().map(TagEntry::id).collect::<Vec<_>>();

    let note_entry = make_test_note(&db, "test0")?;
    tag_ops.add_to(note_entry.id, &created_ids)?;

    let mut note_tags = tag_ops.get_tags_for_note(note_entry.id)?;
    note_tags.sort_unstable_by_key(TagEntry::id);

    assert_eq!(note_tags, created);

    let summary = tag_ops.set_note_tags(note_entry.id, HashSet::new())?;

    assert_eq!(summary.removed, 3);
    assert_eq!(summary.added, 0);
    assert_eq!(summary.created, 0);

    Ok(())
}

#[test]
fn set_tags_add_new_tags() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();

    let x = tag_set!["one", "two", "three"];

    let note_entry = make_test_note(&db, "test0")?;

    let summary = tag_ops.set_note_tags(note_entry.id, x.clone())?;

    assert_eq!(summary.removed, 0);
    assert_eq!(summary.added, 3);
    assert_eq!(summary.created, 3);

    let note_tags = tag_ops
        .get_tags_for_note(note_entry.id)?
        .into_iter()
        .map(|e| e.tag_name)
        .collect::<HashSet<_>>();

    assert_eq!(note_tags, x);

    Ok(())
}

#[test]
fn set_tags_add_existing_tags() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();

    let x = tag_set!["one", "two", "three"];

    tag_ops.ensure_exist(x.clone())?;

    let note_entry = make_test_note(&db, "test0")?;

    let summary = tag_ops.set_note_tags(note_entry.id, x.clone())?;

    assert_eq!(summary.removed, 0);
    assert_eq!(summary.added, 3);
    assert_eq!(summary.created, 0);

    let note_tags = tag_ops
        .get_tags_for_note(note_entry.id)?
        .into_iter()
        .map(|e| e.tag_name)
        .collect::<HashSet<_>>();

    assert_eq!(note_tags, x);

    Ok(())
}

#[test]
fn set_tags_create_one_add_one_remove_one_leave_one_alone() -> Fallible<()> {
    let db = get_mem_db();
    let tag_ops = db.tag_ops();

    //one gets removed - so it needs to exist and be added - then not set
    //two gets left alone - so it needs to exist and be added - then set
    //three gets added - so it needs to just exist - then set
    //four gets created - so it needs to not exist - then set

    let created = tag_ops.create(&tag_set!("one", "two", "three"))?;

    let one_id = created
        .iter()
        .find(|e| e.tag_name == TagName::from("one"))
        .map(TagEntry::id)
        .expect("tag one should have been created!");

    let two_id = created
        .iter()
        .find(|e| e.tag_name == TagName::from("two"))
        .map(TagEntry::id)
        .expect("tag one should have been created!");

    let note_entry = make_test_note(&db, "test0")?;

    tag_ops.add_to(note_entry.id, &vec![one_id, two_id])?;

    let summary = tag_ops.set_note_tags(note_entry.id, tag_set!["two", "three", "four"])?;

    assert_eq!(summary.removed, 1);
    //two are added but one of them exists already
    assert_eq!(summary.added, 2);
    assert_eq!(summary.created, 1);

    let note_tags = tag_ops
        .get_tags_for_note(note_entry.id)?
        .into_iter()
        .map(|e| e.tag_name)
        .collect::<HashSet<_>>();

    assert_eq!(note_tags, tag_set!("two", "three", "four"));

    Ok(())
}
