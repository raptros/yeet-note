use std::collections::HashSet;

use diesel::prelude::*;

use yn_utils::ResultConvert;

use crate::connect_result::DbQueryResult;
use crate::notes::NoteKey;
use crate::tags::model::NoteToTagEntry;
use crate::Db;

use self::model::TagEntry;
pub use self::model::TagKey;
pub use self::model::TagName;

pub mod model;

#[cfg(test)]
pub(crate) mod tests;

pub struct TagOps<'a> {
    pub db: &'a Db,
}

impl<'a> From<&'a Db> for TagOps<'a> {
    fn from(db: &'a Db) -> Self {
        TagOps { db }
    }
}

impl<'a> TagOps<'a> {
    /// this operation updates the tag records so that the target note is paired with just the
    /// the passed tags
    /// in the first stage, it uses `self.ensure_exists()`. in the second, it updates the tag
    /// relation table by listing, removing, and adding, all in a transaction.
    ///
    /// it takes a HashSet for two reasons
    /// * tag-set needs to be deduplicated at some point anyway. might as well be upstream of here
    /// * hashset provides useful perf characteristics
    ///
    /// it returns a triple with the change counts.
    pub fn set_note_tags(
        &self,
        note_key: NoteKey,
        tags: HashSet<TagName>,
    ) -> DbQueryResult<SetTagCounts> {
        //cut out early if tags is empty - just remove them all and get out
        if tags.is_empty() {
            return self
                .remove_all_from(note_key)
                .map(|t| SetTagCounts::default().set_removed(t));
        }

        // intentionally creating tags in a separate txn from the relation management txn
        let ensure_tag_result = self.ensure_exist(tags)?;

        let created = ensure_tag_result
            .created
            .iter()
            .map(TagEntry::id)
            .collect::<Vec<_>>();

        //if no tags previously existed, we can add all the created tags and get out
        if ensure_tag_result.existed.is_empty() {
            let count = ensure_tag_result.created.len();
            self.add_to(note_key, &created)?;
            return Ok(SetTagCounts::default().set_added(count).set_created(count));
        }

        let mut counts = SetTagCounts::default();
        counts = counts.set_created(ensure_tag_result.created.len());

        let existed = ensure_tag_result
            .existed
            .iter()
            .map(TagEntry::id)
            .collect::<HashSet<_>>();

        self.db.transaction(move || {
            let note_tags = self
                .list_for_note(note_key)?
                .into_iter()
                .collect::<HashSet<TagKey>>();

            let tags_del = note_tags
                .difference(&existed)
                .map(|k| *k)
                .collect::<Vec<TagKey>>();

            counts = counts.set_removed(tags_del.len());

            if !tags_del.is_empty() {
                self.remove_from(note_key, &tags_del)?;
            }

            let tags_to_add = existed
                .difference(&note_tags)
                .chain(created.iter())
                .map(TagKey::clone)
                .collect::<Vec<_>>();

            counts = counts.set_added(tags_to_add.len());

            if !tags_to_add.is_empty() {
                self.add_to(note_key, &tags_to_add)?;
            }

            Ok(counts)
        })
    }

    /// Lists the tag ids related to the identified note
    pub fn list_for_note(&self, note_key: NoteKey) -> DbQueryResult<Vec<TagKey>> {
        use crate::schema::notes_to_tags::dsl::*;

        notes_to_tags
            .select(tag_id)
            .filter(note_id.eq(note_key))
            .load(self.db.conn())
            .from_err()
    }

    /// Lists the tags related to the specified note. joins.
    pub fn get_tags_for_note(&self, note_key: NoteKey) -> DbQueryResult<Vec<TagEntry>> {
        use crate::schema::notes_to_tags;
        use crate::schema::tags;

        tags::table
            .inner_join(notes_to_tags::table)
            .select((tags::id, tags::tag_name))
            .filter(notes_to_tags::note_id.eq(note_key))
            .load(self.db.conn())
            .from_err()
    }

    /// removes the identified tags from the relation for the identified note
    pub fn remove_from(&self, note_key: NoteKey, del: &Vec<TagKey>) -> DbQueryResult<usize> {
        use crate::schema::notes_to_tags::dsl::*;
        let q = notes_to_tags.filter(note_id.eq(note_key).and(tag_id.eq_any(del)));
        diesel::delete(q).execute(self.db.conn()).from_err()
    }

    /// Removes all tag relations from the identified note.
    pub fn remove_all_from(&self, note_key: NoteKey) -> DbQueryResult<usize> {
        use crate::schema::notes_to_tags::dsl::*;
        let q = notes_to_tags.filter(note_id.eq(note_key));
        diesel::delete(q).execute(self.db.conn()).from_err()
    }

    /// Adds a set of tags (by key) to the identified note.
    /// will error if any of those tags are already on the note.
    /// make sure to eliminate dupes before calling
    pub fn add_to(&self, note_key: NoteKey, add: &Vec<TagKey>) -> DbQueryResult<()> {
        use crate::schema::notes_to_tags::dsl::*;

        let insert_values = add
            .iter()
            .map(|t| NoteToTagEntry::new(note_key, *t))
            .collect::<Vec<_>>();

        diesel::insert_into(notes_to_tags)
            .values(&insert_values)
            .execute(self.db.conn())
            .void_res()
            .from_err()
    }

    /// determines which of the tags exist already, creates any of the ones that do not
    ///
    /// runs `query_names()` and `create()` inside a transaction
    pub fn ensure_exist(&self, mut tags: HashSet<TagName>) -> DbQueryResult<EnsureTagsResult> {
        if tags.is_empty() {
            return Ok(EnsureTagsResult::default());
        }
        self.db.transaction(|| {
            let existed = self.query_names(&tags)?;

            for e in existed.iter() {
                tags.remove(e.tag_name());
            }

            let created = self.create(&tags)?;

            Ok(EnsureTagsResult { created, existed })
        })
    }

    /// looks up tags by name, returning the ones that exist
    pub fn query_names<'b, T>(&self, tag_names: T) -> DbQueryResult<Vec<TagEntry>>
    where
        T: IntoIterator<Item = &'b TagName>,
    {
        use crate::schema::tags::dsl::*;
        tags.filter(tag_name.eq_any(tag_names))
            .order(id.asc())
            .load(self.db.conn())
            .from_err()
    }

    /// look up a single tag by name.
    pub fn get_named(&self, name: &TagName) -> DbQueryResult<Option<TagKey>> {
        use crate::schema::tags::dsl::*;
        tags.filter(tag_name.eq(name))
            .select(id)
            .first(self.db.conn())
            .optional()
            .from_err()
    }

    /// creates the tags. will error out if any of them exist already.
    pub fn create(&self, new_tags: &HashSet<TagName>) -> DbQueryResult<Vec<TagEntry>> {
        use crate::schema::tags::dsl::*;
        self.db.transaction(|| {
            let tag_vec = new_tags.iter().collect::<Vec<_>>();
            diesel::insert_into(tags)
                .values(tag_vec)
                .execute(self.db.conn())?;
            //unwrap the tag names
            self.query_names(new_tags)
        })
    }

    /// look up a tag entry by the id
    pub fn try_by_id(&self, tag_key: TagKey) -> DbQueryResult<Option<TagEntry>> {
        use crate::schema::tags::dsl::*;
        tags.find(tag_key)
            .first(self.db.conn())
            .optional()
            .from_err()
    }

    /// gets the note ids for a key id
    pub fn get_note_ids_for_tag_id(&self, tag_key: TagKey) -> DbQueryResult<Vec<NoteKey>> {
        use crate::schema::notes_to_tags::dsl::*;
        notes_to_tags
            .filter(tag_id.eq(tag_key))
            .select(note_id)
            .load(self.db.conn())
            .from_err()
    }

    /// get the notes related to some tag id.
    /// todo: obviously sorting still needs to be worked out.
    pub fn get_notes_for_tag(
        &self,
        tag_key: TagKey,
    ) -> DbQueryResult<Vec<crate::notes::model::NoteEntry>> {
        use crate::schema::notes;
        use crate::schema::notes_to_tags;

        notes_to_tags::table
            .inner_join(notes::table.on(notes::id.eq(notes_to_tags::note_id)))
            .select(notes::all_columns)
            .filter(notes_to_tags::tag_id.eq(tag_key))
            .load(self.db.conn())
            .from_err()
    }
}

#[derive(Default)]
pub struct SetTagCounts {
    pub created: usize,
    pub added: usize,
    pub removed: usize,
}

impl SetTagCounts {
    fn set_created(mut self, v: usize) -> Self {
        self.created = v;
        self
    }

    fn set_added(mut self, v: usize) -> Self {
        self.added = v;
        self
    }

    fn set_removed(mut self, v: usize) -> Self {
        self.removed = v;
        self
    }
}

#[derive(Default)]
pub struct EnsureTagsResult {
    pub created: Vec<TagEntry>,
    pub existed: Vec<TagEntry>,
}
