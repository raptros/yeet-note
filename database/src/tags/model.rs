use crate::notes::NoteKey;
use crate::schema::notes_to_tags;
use crate::schema::tags;

#[derive(Debug, Clone, Copy, PartialOrd, Ord, Hash, PartialEq, Eq, DieselNewType)]
pub struct TagKey(i64);

#[derive(Debug, Clone, Hash, PartialEq, Eq, PartialOrd, Ord, DieselNewType, Insertable)]
#[table_name = "tags"]
pub struct TagName(#[column_name = "tag_name"] String);

impl<S> From<S> for TagName
where
    S: Into<String>,
{
    fn from(s: S) -> Self {
        let mut stringed = s.into();
        //todo: perform various normalizations
        stringed.make_ascii_lowercase();
        TagName(stringed)
    }
}

#[derive(Queryable, Debug, Identifiable, Hash, PartialEq, Eq)]
#[table_name = "tags"]
pub struct TagEntry {
    pub id: TagKey,
    pub tag_name: TagName,
}

impl TagEntry {
    pub fn id(&self) -> TagKey {
        self.id
    }

    pub fn clone_tag_name(&self) -> TagName {
        self.tag_name.clone()
    }

    pub fn tag_name(&self) -> &TagName {
        &self.tag_name
    }
}

//we don't need this because we can just insert TagName directly
/*
#[derive(Debug, Insertable)]
#[table_name = "tags"]
pub struct NewTag {
    pub tag_name: TagName,
}
*/

//there isn't really any need to update tags at this point

// a notes<->tags relation can be created, queried, and deleted
#[derive(Queryable, Debug, Insertable)]
#[table_name = "notes_to_tags"]
pub struct NoteToTagEntry {
    pub note_id: super::NoteKey,
    pub tag_id: TagKey,
}

impl NoteToTagEntry {
    pub fn new(note_id: super::NoteKey, tag_id: TagKey) -> Self {
        NoteToTagEntry { note_id, tag_id }
    }
}
