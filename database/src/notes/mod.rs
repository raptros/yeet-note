use chrono::NaiveDate;
use diesel::prelude::*;
use diesel::result::OptionalExtension;

use yn_utils::ResultConvert;

use crate::file_index::FileIndexKey;
use crate::Db;
use crate::DbQueryError;
use crate::DbQueryResult;

use self::model::NewNoteEntry;
use self::model::NoteEntry;
pub use self::model::NoteKey;
use self::model::UpdateNoteData;
use self::model::UpdateNoteEntry;

pub mod model;

#[cfg(test)]
pub(crate) mod tests;

pub struct NoteOps<'a> {
    pub db: &'a Db,
}

impl<'a> From<&'a Db> for NoteOps<'a> {
    fn from(db: &'a Db) -> Self {
        NoteOps { db }
    }
}

impl<'a> NoteOps<'a> {
    pub fn insert_note(&self, entry: NewNoteEntry) -> DbQueryResult<NoteKey> {
        use crate::schema::notes::dsl::*;

        self.db.transaction::<NoteKey, DbQueryError, _>(|| {
            diesel::insert_into(notes)
                .values(entry)
                .execute(&*self.db.conn())?;
            notes
                .select(id)
                .order(id.desc())
                .first(&*self.db.conn())
                .from_err()
        })
    }

    /// call this for new fs entries - checks for existing notes for adjacent fs entries.
    ///
    /// note that this can't wipe out an existing date, but we basically never want to do that anyway.
    pub fn add_note_for_file(&self, file_data: NoteFileData) -> DbQueryResult<NoteKey> {
        self.db.transaction(|| {
            let parent_note = self.get_note_by_directory(file_data.parent_dir_id)?;
            let existing_note = self.lookup_named_note(parent_note.id, &file_data.name)?;

            match existing_note {
                None => {
                    let new_note = NewNoteEntry::for_data(parent_note.id, file_data);
                    self.insert_note(new_note)
                }
                Some(existing_note) => {
                    let update = existing_note.into_update(file_data);
                    self.update_note(&update)?;
                    Ok(update.id)
                }
            }
        })
    }

    pub fn list_child_notes(&self, parent_key: NoteKey) -> DbQueryResult<Vec<NoteEntry>> {
        use crate::schema::notes::dsl::*;
        notes
            .filter(parent_note_id.eq(parent_key))
            //there should be only one self-parented note
            .filter(id.ne(NoteKey::ROOT))
            .load(self.db.conn())
            .from_err()
    }

    pub fn update_note(&self, update_model: &UpdateNoteEntry) -> DbQueryResult<()> {
        diesel::update(update_model)
            .set(update_model)
            .execute(self.db.conn())
            .void_res()
            .from_err()
    }

    pub fn update_note_data_by_md_file(
        &self,
        file_key: FileIndexKey,
        data: UpdateNoteData,
    ) -> DbQueryResult<()> {
        use crate::schema::notes::dsl::*;
        if data.is_noop() {
            return Ok(());
        }
        diesel::update(notes.filter(md_file_id.eq(file_key)))
            .set(data)
            .execute(self.db.conn())
            .void_res()
            .from_err()
    }

    /// this should only be used if it's reasonable to expect that the directory has already been indexed.
    pub fn get_note_by_directory(&self, dir_key: FileIndexKey) -> DbQueryResult<NoteEntry> {
        use crate::schema::notes::dsl::*;
        notes
            .filter(directory_file_id.eq(dir_key))
            .first(self.db.conn())
            .from_err()
    }

    pub fn get_note_by_md_file(&self, file_key: FileIndexKey) -> DbQueryResult<NoteEntry> {
        use crate::schema::notes::dsl::*;
        notes
            .filter(md_file_id.eq(file_key))
            .first(self.db.conn())
            .from_err()
    }

    pub fn lookup_named_note(
        &self,
        parent_note: NoteKey,
        name: &str,
    ) -> DbQueryResult<Option<NoteEntry>> {
        use crate::schema::notes::dsl::*;
        notes
            .filter(parent_note_id.eq(parent_note).and(note_name.eq(name)))
            .first(self.db.conn())
            .optional()
            .from_err()
    }

    pub fn get_note_by_id(&self, note_key: NoteKey) -> DbQueryResult<NoteEntry> {
        use crate::schema::notes::dsl::*;
        notes.find(note_key).first(&*self.db.conn()).from_err()
    }

    pub fn try_get_note_by_id(&self, note_key: NoteKey) -> DbQueryResult<Option<NoteEntry>> {
        use crate::schema::notes::dsl::*;
        notes
            .find(note_key)
            .first(&*self.db.conn())
            .optional()
            .from_err()
    }
}

/// data specific to a note file or path to go into the `notes` table.
/// put differently, this should contain the values that should be stored into the `notes` table
/// for whatever note this is
pub struct NoteFileData {
    pub name: String,
    pub date: Option<NaiveDate>,
    pub parent_dir_id: FileIndexKey,
    pub file_index_id: FileIndexKey,
    pub entry_type: NoteEntryType,
}

impl NoteFileData {
    pub fn get_file_fields(&self) -> NoteFileEntryFields {
        let mut fields = NoteFileEntryFields::default();
        match self.entry_type {
            NoteEntryType::Directory => fields.directory_file_id = Some(self.file_index_id),
            NoteEntryType::Markdown => fields.md_file_id = Some(self.file_index_id),
        };
        fields
    }
}

pub enum NoteEntryType {
    Directory,
    Markdown,
}

#[derive(Default)]
pub struct NoteFileEntryFields {
    pub directory_file_id: Option<FileIndexKey>,
    pub md_file_id: Option<FileIndexKey>,
}
