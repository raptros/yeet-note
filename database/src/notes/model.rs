use chrono::NaiveDate;

use crate::file_index::FileIndexKey;
use crate::notes::NoteFileData;
use crate::schema::notes;

#[derive(Debug, Clone, Copy, PartialOrd, Ord, Hash, PartialEq, Eq, DieselNewType)]
pub struct NoteKey(i64);

impl NoteKey {
    pub const ROOT: Self = NoteKey(1);
}

#[derive(Queryable, Debug, Identifiable)]
#[table_name = "notes"]
pub struct NoteEntry {
    pub id: NoteKey,
    pub parent_note_id: NoteKey,
    pub note_name: String,
    pub note_date: Option<NaiveDate>,
    pub directory_file_id: Option<FileIndexKey>,
    pub md_file_id: Option<FileIndexKey>,
}

impl NoteEntry {
    pub fn into_update(self, note_data: NoteFileData) -> UpdateNoteEntry {
        let fields = note_data.get_file_fields();

        UpdateNoteEntry {
            id: self.id,
            note_name: self.note_name,
            note_date: note_data.date,
            directory_file_id: fields.directory_file_id,
            md_file_id: fields.md_file_id,
        }
    }
}

#[derive(Debug, Insertable, Clone)]
#[table_name = "notes"]
pub struct NewNoteEntry {
    pub parent_note_id: NoteKey,
    pub note_name: String,
    pub note_date: Option<NaiveDate>,
    pub directory_file_id: Option<FileIndexKey>,
    pub md_file_id: Option<FileIndexKey>,
}

impl NewNoteEntry {
    pub fn for_data(parent_note_id: NoteKey, note_data: NoteFileData) -> Self {
        let fields = note_data.get_file_fields();
        NewNoteEntry {
            parent_note_id,
            note_name: note_data.name,
            note_date: note_data.date,
            directory_file_id: fields.directory_file_id,
            md_file_id: fields.md_file_id,
        }
    }
}

/// allows updating fields that make sense to update
/// diesel's behavior wrt optionals, where it treats None as "don't make a change", is important
/// for how this functions - leave the *_file_id fields empty allows setting one of the fields w/o
/// needing to copy the existing value of the other
///
/// keep in mind that this means that it won't null out a recorded date. however, i don't think
/// that's an issue - see `NoteOps::add_note_for_file()`
#[derive(AsChangeset, Identifiable)]
#[table_name = "notes"]
pub struct UpdateNoteEntry {
    pub id: NoteKey,

    // i am not sure how the note name would change, but ....
    pub note_name: String,

    //this ofc can change
    pub note_date: Option<NaiveDate>,

    pub directory_file_id: Option<FileIndexKey>,
    pub md_file_id: Option<FileIndexKey>,
}

#[derive(AsChangeset)]
#[table_name = "notes"]
pub struct UpdateNoteData {
    //this ofc can change
    pub note_date: Option<NaiveDate>,
}

impl UpdateNoteData {
    pub fn is_noop(&self) -> bool {
        self.note_date.is_none()
    }
}
