use diesel::result::DatabaseErrorKind;
use diesel::result::Error as DieselError;
use failure::bail;
use failure::format_err;
use failure::Fail;
use failure::Fallible;
use failure::ResultExt;

use directory_scan::fs;
use yn_utils::ResultConvert;

use crate::file_index::tests::fake_metadata;
use crate::file_index::FileIndexKey;
use crate::notes::model::NewNoteEntry;
use crate::notes::model::UpdateNoteData;
use crate::notes::NoteEntryType;
use crate::notes::NoteFileData;
use crate::notes::NoteKey;
use crate::notes::NoteOps;
use crate::tests::get_mem_db;
use crate::Db;

pub(crate) fn create_markdown_file(
    db: &Db,
    name: &str,
    parent_id: FileIndexKey,
) -> Fallible<FileIndexKey> {
    let mut path_buf = db
        .file_index()
        .get_path(parent_id)
        .context("create_markdown_file: failed to get parent of file being inserted for test")?;

    path_buf.push(name);
    path_buf.set_extension(".md");

    let depth = path_buf.components().count() as i64;

    let next_entry = fs::Entry::new(path_buf, depth, fs::EntryType::File, fake_metadata());

    db.file_index()
        .insert(parent_id, &next_entry)
        .context("create_markdown_file: insert of new entry failed")
        .from_err()
}

pub(crate) fn create_dir_entry(
    db: &Db,
    name: &str,
    parent_id: FileIndexKey,
) -> Fallible<FileIndexKey> {
    let mut path_buf = db
        .file_index()
        .get_path(parent_id)
        .context("create_dir_entry: failed to get parent of dir being inserted for test")?;

    path_buf.push(name);

    let depth = path_buf.components().count() as i64;

    let next_entry = fs::Entry::new(path_buf, depth, fs::EntryType::Directory, fake_metadata());

    db.file_index()
        .insert(parent_id, &next_entry)
        .context("create_dir_entry: insert of new entry failed")
        .from_err()
}

#[test]
fn check_root() {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let note_root = table
        .get_note_by_id(NoteKey::ROOT)
        .expect("failed to get root note");

    assert_eq!(note_root.id, NoteKey::ROOT);
    assert_eq!(note_root.parent_note_id, NoteKey::ROOT);
    assert_eq!(note_root.note_date, None);
    assert_eq!(note_root.md_file_id, None);
    assert_eq!(note_root.directory_file_id, Some(FileIndexKey::ROOT));

    let note_root_by_dir = table
        .get_note_by_directory(FileIndexKey::ROOT)
        .expect("failed to get root note by directory");

    assert_eq!(note_root_by_dir.id, NoteKey::ROOT);
    assert_eq!(note_root_by_dir.parent_note_id, NoteKey::ROOT);
    assert_eq!(note_root_by_dir.note_date, None);
    assert_eq!(note_root_by_dir.md_file_id, None);
    assert_eq!(note_root_by_dir.directory_file_id, Some(FileIndexKey::ROOT));
}

#[test]
fn add_get_file_note() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let file_id = create_markdown_file(&db, "test0", FileIndexKey::ROOT)?;

    let file_note_data = NoteFileData {
        name: "test0".to_string(),
        date: None,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: file_id,
        entry_type: NoteEntryType::Markdown,
    };

    let new_note_id = table
        .add_note_for_file(file_note_data)
        .context("failed to insert new note!")?;

    let created = table
        .get_note_by_id(new_note_id)
        .context("failed to get inserted note")?;

    assert_eq!(created.id, new_note_id);
    assert_eq!(created.parent_note_id, NoteKey::ROOT);
    assert_eq!(created.note_date, None);
    assert_eq!(created.md_file_id, Some(file_id));
    assert_eq!(created.directory_file_id, None);

    let re_fetch = table.get_note_by_md_file(file_id)?;

    assert_eq!(re_fetch.id, new_note_id);

    let get_by_name = table
        .lookup_named_note(NoteKey::ROOT, "test0")?
        .map_or(Err(format_err!("lookup_named_note returned empty!")), Ok)?;

    assert_eq!(get_by_name.id, new_note_id);

    let parent_listing = table.list_child_notes(NoteKey::ROOT)?;

    assert_eq!(parent_listing.len(), 1);

    assert_eq!(parent_listing[0].id, new_note_id);

    Ok(())
}

#[test]
fn add_modify_file_note() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let file_id = create_markdown_file(&db, "test0", FileIndexKey::ROOT)?;

    let file_note_data = NoteFileData {
        name: "test0".to_string(),
        date: None,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: file_id,
        entry_type: NoteEntryType::Markdown,
    };

    let new_note_id = table
        .add_note_for_file(file_note_data)
        .context("failed to insert new note!")?;

    let created = table
        .get_note_by_id(new_note_id)
        .context("failed to get inserted note")?;

    assert_eq!(created.id, new_note_id);
    assert_eq!(created.parent_note_id, NoteKey::ROOT);
    assert_eq!(created.note_date, None);
    assert_eq!(created.md_file_id, Some(file_id));
    assert_eq!(created.directory_file_id, None);

    let date = chrono::NaiveDate::from_ymd_opt(2018, 12, 29);

    let update = UpdateNoteData { note_date: date };

    table.update_note_data_by_md_file(file_id, update)?;

    let updated = table
        .get_note_by_id(new_note_id)
        .context("failed to get inserted note")?;

    assert_eq!(updated.id, new_note_id);
    assert_eq!(updated.parent_note_id, NoteKey::ROOT);
    assert_eq!(updated.note_date, date);
    assert_eq!(updated.md_file_id, Some(file_id));
    assert_eq!(updated.directory_file_id, None);

    Ok(())
}

#[test]
fn add_delete_file_note() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let file_id = create_markdown_file(&db, "test0", FileIndexKey::ROOT)?;

    let file_note_data = NoteFileData {
        name: "test0".to_string(),
        date: None,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: file_id,
        entry_type: NoteEntryType::Markdown,
    };

    let new_note_id = table
        .add_note_for_file(file_note_data)
        .context("failed to insert new note!")?;

    let created = table
        .get_note_by_id(new_note_id)
        .context("failed to get inserted note")?;

    assert_eq!(created.id, new_note_id);
    assert_eq!(created.parent_note_id, NoteKey::ROOT);
    assert_eq!(created.note_date, None);
    assert_eq!(created.md_file_id, Some(file_id));
    assert_eq!(created.directory_file_id, None);

    db.file_index().remove(file_id)?;

    let res = table.try_get_note_by_id(new_note_id)?;
    assert!(res.is_none());

    Ok(())
}

#[test]
fn add_get_dir_note() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let dir_id = create_dir_entry(&db, "test0", FileIndexKey::ROOT)?;

    let dir_note_data = NoteFileData {
        name: "test0".to_string(),
        date: None,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: dir_id,
        entry_type: NoteEntryType::Directory,
    };

    let new_note_id = table
        .add_note_for_file(dir_note_data)
        .context("failed to insert new note!")?;

    let created = table
        .get_note_by_id(new_note_id)
        .context("failed to get inserted note")?;

    assert_eq!(created.id, new_note_id);
    assert_eq!(created.parent_note_id, NoteKey::ROOT);
    assert_eq!(created.note_date, None);
    assert_eq!(created.md_file_id, None);
    assert_eq!(created.directory_file_id, Some(dir_id));

    let re_fetch = table.get_note_by_directory(dir_id)?;

    assert_eq!(re_fetch.id, new_note_id);

    let get_by_name = table
        .lookup_named_note(NoteKey::ROOT, "test0")?
        .map_or(Err(format_err!("lookup_named_note returned empty!")), Ok)?;

    assert_eq!(get_by_name.id, new_note_id);

    Ok(())
}

#[test]
fn add_delete_dir_note() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let dir_id = create_dir_entry(&db, "test0", FileIndexKey::ROOT)?;

    let dir_note_data = NoteFileData {
        name: "test0".to_string(),
        date: None,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: dir_id,
        entry_type: NoteEntryType::Directory,
    };

    let new_note_id = table
        .add_note_for_file(dir_note_data)
        .context("failed to insert new note!")?;

    let created = table
        .get_note_by_id(new_note_id)
        .context("failed to get inserted note")?;

    assert_eq!(created.id, new_note_id);
    assert_eq!(created.parent_note_id, NoteKey::ROOT);
    assert_eq!(created.note_date, None);
    assert_eq!(created.md_file_id, None);
    assert_eq!(created.directory_file_id, Some(dir_id));

    db.file_index().remove(dir_id)?;

    let res = table.try_get_note_by_id(new_note_id)?;
    assert!(res.is_none());

    Ok(())
}

#[test]
fn add_file_then_dir() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let date = chrono::NaiveDate::from_ymd_opt(2018, 12, 29);

    let file_id = create_markdown_file(&db, "test0", FileIndexKey::ROOT)?;

    let file_note_data = NoteFileData {
        name: "test0".to_string(),
        date,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: file_id,
        entry_type: NoteEntryType::Markdown,
    };

    let file_note_id = table
        .add_note_for_file(file_note_data)
        .context("failed to insert file note!")?;

    let dir_id = create_dir_entry(&db, "test0", FileIndexKey::ROOT)?;

    let dir_note_data = NoteFileData {
        name: "test0".to_string(),
        date: None,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: dir_id,
        entry_type: NoteEntryType::Directory,
    };

    let dir_note_id = table
        .add_note_for_file(dir_note_data)
        .context("failed to insert dir note!")?;

    assert_eq!(file_note_id, dir_note_id);

    let created = table
        .get_note_by_id(file_note_id)
        .context("failed to get inserted note")?;

    assert_eq!(created.id, file_note_id);
    assert_eq!(created.parent_note_id, NoteKey::ROOT);
    assert_eq!(created.note_date, date);
    assert_eq!(created.md_file_id, Some(file_id));
    assert_eq!(created.directory_file_id, Some(dir_id));

    Ok(())
}

#[test]
fn must_have_file_or_dir() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let new_note = NewNoteEntry {
        parent_note_id: NoteKey::ROOT,
        note_name: "invalid note".to_string(),
        note_date: None,
        //these next two are why this should fail
        directory_file_id: None,
        md_file_id: None,
    };
    let result = table
        .insert_note(new_note)
        .expect_err("expected insert_note to fail here");

    let cause = result
        .cause()
        .expect("cause should be present")
        .downcast_ref::<DieselError>()
        .expect("cause should be a diesel error");

    match cause {
        DieselError::DatabaseError(_kind, info_box) => {
            assert_eq!(
                info_box.message(),
                "note must be created with at least one file reference"
            );
        }
        _ => bail!("got some other kind of diesel error: {:?}", cause),
    }

    let children = table.list_child_notes(NoteKey::ROOT)?;

    assert_eq!(children.len(), 0);

    Ok(())
}

#[test]
fn name_and_parent_is_unique() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let name = "duplicated note";

    let first_file_id = create_markdown_file(&db, "first_note.md", FileIndexKey::ROOT)?;

    let first_note = NewNoteEntry {
        parent_note_id: NoteKey::ROOT,
        note_name: name.to_string(),
        note_date: None,
        directory_file_id: None,
        md_file_id: Some(first_file_id),
    };

    let first_result = table.insert_note(first_note)?;

    //todo: right now the file index has no parent+name uniqueness constraint. this is probably a problem.
    let second_file_id = create_markdown_file(&db, "second_note.md", FileIndexKey::ROOT)?;

    let second_note = NewNoteEntry {
        parent_note_id: NoteKey::ROOT,
        note_name: name.to_string(),
        note_date: None,
        directory_file_id: None,
        md_file_id: Some(second_file_id),
    };

    let second_result = table
        .insert_note(second_note)
        .expect_err("second insert should have failed!");

    let cause = second_result
        .cause()
        .expect("cause should be present")
        .downcast_ref::<DieselError>()
        .expect("cause should be a diesel error");

    match cause {
        DieselError::DatabaseError(DatabaseErrorKind::UniqueViolation, _info_box) => (),
        _ => bail!("got some other kind of diesel error: {:?}", cause),
    }

    let children = table.list_child_notes(NoteKey::ROOT)?;

    assert_eq!(children.len(), 1);

    assert_eq!(children[0].id, first_result);

    Ok(())
}

#[test]
fn note_directory_is_unique() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let dir_id = create_dir_entry(&db, "directory", FileIndexKey::ROOT)?;

    let first_note = NewNoteEntry {
        parent_note_id: NoteKey::ROOT,
        note_name: "first note".to_string(),
        note_date: None,
        directory_file_id: Some(dir_id),
        md_file_id: None,
    };

    let first_result = table.insert_note(first_note)?;

    let second_note = NewNoteEntry {
        parent_note_id: NoteKey::ROOT,
        note_name: "second note".to_string(),
        note_date: None,
        directory_file_id: Some(dir_id),
        md_file_id: None,
    };

    let second_result = table
        .insert_note(second_note)
        .expect_err("second insert should have failed!");

    let cause = second_result
        .cause()
        .expect("cause should be present")
        .downcast_ref::<DieselError>()
        .expect("cause should be a diesel error");

    match cause {
        DieselError::DatabaseError(DatabaseErrorKind::UniqueViolation, _info_box) => (),
        _ => bail!("got some other kind of diesel error: {:?}", cause),
    }

    let children = table.list_child_notes(NoteKey::ROOT)?;

    assert_eq!(children.len(), 1);

    assert_eq!(children[0].id, first_result);

    Ok(())
}

#[test]
fn note_md_file_is_unique() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let file_id = create_markdown_file(&db, "first_note.md", FileIndexKey::ROOT)?;

    let first_note = NewNoteEntry {
        parent_note_id: NoteKey::ROOT,
        note_name: "first note".to_string(),
        note_date: None,
        directory_file_id: None,
        md_file_id: Some(file_id),
    };

    let first_result = table.insert_note(first_note)?;

    let second_note = NewNoteEntry {
        parent_note_id: NoteKey::ROOT,
        note_name: "second note".to_string(),
        note_date: None,
        directory_file_id: None,
        md_file_id: Some(file_id),
    };

    let second_result = table
        .insert_note(second_note)
        .expect_err("second insert should have failed!");

    let cause = second_result
        .cause()
        .expect("cause should be present")
        .downcast_ref::<DieselError>()
        .expect("cause should be a diesel error");

    match cause {
        DieselError::DatabaseError(DatabaseErrorKind::UniqueViolation, _info_box) => (),
        _ => bail!("got some other kind of diesel error: {:?}", cause),
    }

    let children = table.list_child_notes(NoteKey::ROOT)?;

    assert_eq!(children.len(), 1);

    assert_eq!(children[0].id, first_result);

    Ok(())
}

#[test]
fn add_dir_then_file() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let date = chrono::NaiveDate::from_ymd_opt(2018, 12, 29);

    let dir_id = create_dir_entry(&db, "test0", FileIndexKey::ROOT)?;

    let dir_note_data = NoteFileData {
        name: "test0".to_string(),
        date: None,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: dir_id,
        entry_type: NoteEntryType::Directory,
    };

    let dir_note_id = table
        .add_note_for_file(dir_note_data)
        .context("failed to insert dir note!")?;

    let file_id = create_markdown_file(&db, "test0", FileIndexKey::ROOT)?;

    let file_note_data = NoteFileData {
        name: "test0".to_string(),
        date,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: file_id,
        entry_type: NoteEntryType::Markdown,
    };

    let file_note_id = table
        .add_note_for_file(file_note_data)
        .context("failed to insert file note!")?;

    assert_eq!(file_note_id, dir_note_id);

    let created = table
        .get_note_by_id(file_note_id)
        .context("failed to get inserted note")?;

    assert_eq!(created.id, file_note_id);
    assert_eq!(created.parent_note_id, NoteKey::ROOT);
    assert_eq!(created.note_date, date);
    assert_eq!(created.md_file_id, Some(file_id));
    assert_eq!(created.directory_file_id, Some(dir_id));

    Ok(())
}

#[test]
fn add_dir_and_file_and_delete_file_then_dir() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let date = chrono::NaiveDate::from_ymd_opt(2018, 12, 29);

    let dir_id = create_dir_entry(&db, "test0", FileIndexKey::ROOT)?;

    let dir_note_data = NoteFileData {
        name: "test0".to_string(),
        date: None,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: dir_id,
        entry_type: NoteEntryType::Directory,
    };

    let dir_note_id = table
        .add_note_for_file(dir_note_data)
        .context("failed to insert dir note!")?;

    let file_id = create_markdown_file(&db, "test0", FileIndexKey::ROOT)?;

    let file_note_data = NoteFileData {
        name: "test0".to_string(),
        date,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: file_id,
        entry_type: NoteEntryType::Markdown,
    };

    let file_note_id = table
        .add_note_for_file(file_note_data)
        .context("failed to insert file note!")?;

    assert_eq!(file_note_id, dir_note_id);

    let created = table
        .get_note_by_id(file_note_id)
        .context("failed to get inserted note")?;

    assert_eq!(created.id, file_note_id);
    assert_eq!(created.parent_note_id, NoteKey::ROOT);
    assert_eq!(created.md_file_id, Some(file_id));
    assert_eq!(created.directory_file_id, Some(dir_id));

    db.file_index().remove(file_id)?;

    let still_present = table
        .get_note_by_id(file_note_id)
        .context("note should not have been deleted yet")?;

    assert_eq!(still_present.md_file_id, None);
    assert_eq!(still_present.directory_file_id, Some(dir_id));

    db.file_index().remove(dir_id)?;

    let res = table.try_get_note_by_id(dir_note_id)?;
    assert!(res.is_none());

    Ok(())
}

#[test]
fn add_dir_and_file_and_delete_dir_then_file() -> Fallible<()> {
    let db = get_mem_db();
    let table: NoteOps = (&db).into();

    let date = chrono::NaiveDate::from_ymd_opt(2018, 12, 29);

    let dir_id = create_dir_entry(&db, "test0", FileIndexKey::ROOT)?;

    let dir_note_data = NoteFileData {
        name: "test0".to_string(),
        date: None,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: dir_id,
        entry_type: NoteEntryType::Directory,
    };

    let dir_note_id = table
        .add_note_for_file(dir_note_data)
        .context("failed to insert dir note!")?;

    let file_id = create_markdown_file(&db, "test0", FileIndexKey::ROOT)?;

    let file_note_data = NoteFileData {
        name: "test0".to_string(),
        date,
        parent_dir_id: FileIndexKey::ROOT,
        file_index_id: file_id,
        entry_type: NoteEntryType::Markdown,
    };

    let file_note_id = table
        .add_note_for_file(file_note_data)
        .context("failed to insert file note!")?;

    assert_eq!(file_note_id, dir_note_id);

    let created = table
        .get_note_by_id(file_note_id)
        .context("failed to get inserted note")?;

    assert_eq!(created.id, file_note_id);
    assert_eq!(created.parent_note_id, NoteKey::ROOT);
    assert_eq!(created.md_file_id, Some(file_id));
    assert_eq!(created.directory_file_id, Some(dir_id));

    db.file_index().remove(dir_id)?;

    let still_present = table
        .get_note_by_id(dir_note_id)
        .context("note should not have been deleted yet")?;

    assert_eq!(still_present.md_file_id, Some(file_id));
    assert_eq!(still_present.directory_file_id, None);

    db.file_index().remove(file_id)?;

    let res = table.try_get_note_by_id(file_note_id)?;
    assert!(res.is_none());

    Ok(())
}
