use failure::Context;
use failure::Fail;

pub type DbConnectResult<T> = Result<T, DbConnectError>;

#[derive(Debug)]
pub struct DbConnectError {
    inner: Context<DbConnectErrorKind>,
}

impl Fail for DbConnectError {
    fn cause(&self) -> Option<&Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&failure::Backtrace> {
        self.inner.backtrace()
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
pub enum DbConnectErrorKind {
    #[fail(display = "failed to open database")]
    ConnectFailed,

    #[fail(display = "sqlite refused to enable foreign key support")]
    ForeignKeyEnableFailed,

    #[fail(display = "cannot open sqlite DB at paths that cannot be made into valid UTF-8")]
    NonUTF8Path,

    #[fail(display = "execution of migrations failed")]
    MigrationExecutionFailed,

    #[fail(display = "set up of connection pool failed")]
    PoolCreationFailed,
}

impl std::fmt::Display for DbConnectError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.inner, f)
    }
}

impl DbConnectError {
    pub fn kind(&self) -> DbConnectErrorKind {
        *self.inner.get_context()
    }
}

impl From<DbConnectErrorKind> for DbConnectError {
    fn from(kind: DbConnectErrorKind) -> Self {
        DbConnectError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<DbConnectErrorKind>> for DbConnectError {
    fn from(inner: Context<DbConnectErrorKind>) -> Self {
        DbConnectError { inner }
    }
}

pub type DbQueryResult<T> = Result<T, DbQueryError>;

#[derive(Debug)]
pub struct DbQueryError {
    inner: Context<DbQueryErrorKind>,
}

impl Fail for DbQueryError {
    fn cause(&self) -> Option<&Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&failure::Backtrace> {
        self.inner.backtrace()
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
pub enum DbQueryErrorKind {
    #[fail(display = "an error occurred while executing the query")]
    QueryError,

    #[fail(display = "connection pool returned an error")]
    PoolError,
}

impl std::fmt::Display for DbQueryError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.inner, f)
    }
}

impl DbQueryError {
    pub fn kind(&self) -> DbQueryErrorKind {
        *self.inner.get_context()
    }
}

impl From<DbQueryErrorKind> for DbQueryError {
    fn from(kind: DbQueryErrorKind) -> Self {
        DbQueryError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<DbQueryErrorKind>> for DbQueryError {
    fn from(inner: Context<DbQueryErrorKind>) -> Self {
        DbQueryError { inner }
    }
}

impl From<diesel::result::Error> for DbQueryError {
    fn from(err: diesel::result::Error) -> Self {
        let ctx = err.context(DbQueryErrorKind::QueryError);
        DbQueryError::from(ctx)
    }
}
