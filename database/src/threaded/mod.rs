use std::thread;
use std::thread::JoinHandle;

use crossbeam_channel as cb;
use log::trace;

use crate::connect_result::DbQueryError;
use crate::connect_result::DbQueryResult;
use crate::Db;

use self::file_index::FileIndexHandler;
use self::notes::NoteOpsHandler;

pub mod file_index;
pub mod notes;

/// The DbThread runs a loop in a separate thread where it selects and performs operations coming in
/// from each of the handlers that have been added. it executes this loop until all of the handlers
/// have shut down their channels.
pub struct DbThread {
    db: Db,
    handlers: Vec<MessageHandler>,
}

impl DbThread {
    /// DbThread needs to take ownership of the Db in order to function. Keep in mind that Db is
    /// expected to be Send and not Sync - it's not a good idea to try to have other references to
    /// the Db out there.
    pub fn new(db: Db) -> Self {
        DbThread {
            db,
            handlers: Vec::new(),
        }
    }

    /// Add a handler to be processed in the loop. Take a look at the trait.
    pub fn add_handler<H: Handler>(mut self, h: H) -> Self {
        let handler = h.to_handler();
        self.handlers.push(handler);
        self
    }

    /// Spawns a thread that will repeatedly run the handlers inside of a transaction, until all of
    /// the handlers have been disconnected.
    pub fn run(self) -> JoinHandle<Db> {
        thread::spawn(move || {
            trace!("dbthread: transaction running");

            let mut handlers = Handlers::new(&self.db, self.handlers);
            (&self.db)
                .transaction::<(), DbQueryError, _>(|| {
                    //the main loop
                    while handlers.still_has_handlers() {
                        handlers.select()
                    }
                    trace!("dbthread: shutting down - all handlers disconnected");
                    Ok(())
                })
                .expect("dbthhread: transaction failed! ");

            //todo: return a result here ... either alongside or wrapping the Db - use that to represent failure
            self.db
        })
    }
}

/// wrapper for the handlers list
struct Handlers<'a> {
    db: &'a Db,
    handlers: Vec<MessageHandler>,
}

impl<'a> Handlers<'a> {
    pub fn new(db: &'a Db, handlers: Vec<MessageHandler>) -> Self {
        Handlers { db, handlers }
    }

    /// Perform a Select on the current handlers, handle whichever comes up, remove that handler
    /// if it turns out it got disconnected
    pub fn select(&mut self) {
        let disposition = {
            let mut select = cb::Select::new();

            for src in self.handlers.iter() {
                select = src.recv(select);
            }

            let oper = select.select();

            self.handlers[oper.index()].handle(oper, self.db)
        };

        if let HandlerDisposition::Disconnected(h_index) = disposition {
            self.handlers.remove(h_index);
        }
    }

    pub fn still_has_handlers(&self) -> bool {
        !self.handlers.is_empty()
    }
}

/// All of the handler types that can be added to the DbThread
pub enum MessageHandler {
    FileIndex(FileIndexHandler),
    NoteOps(NoteOpsHandler),
}

/// Represents the status of a handler from a recv use
#[derive(PartialEq, Eq)]
enum HandlerDisposition {
    /// The handler was ok, nothing needs to be done to it
    Fine,

    /// `recv` returned an error for the handler at this index in the `Select`. This pretty much
    /// means one thing - that the other side of the channel has disconnected.
    Disconnected(usize),
}

impl MessageHandler {
    /// add the contained handler to the Select.
    fn recv<'a>(&'a self, mut select: cb::Select<'a>) -> cb::Select<'a> {
        match self {
            MessageHandler::FileIndex(h) => {
                select.recv(h.receiver());
            }
            MessageHandler::NoteOps(h) => {
                select.recv(h.receiver());
            }
        }
        select
    }

    /// This handler was chosen by the Select, so process it.
    fn handle<'a>(&'a self, oper: cb::SelectedOperation<'a>, db: &Db) -> HandlerDisposition {
        let h_index = oper.index();
        match self {
            MessageHandler::FileIndex(ref h) => match oper.recv(h.receiver()) {
                Ok(msg) => {
                    h.handle(msg, db);
                    HandlerDisposition::Fine
                }
                Err(_) => HandlerDisposition::Disconnected(h_index),
            },
            MessageHandler::NoteOps(ref h) => match oper.recv(h.receiver()) {
                Ok(msg) => {
                    h.handle(msg, db);
                    HandlerDisposition::Fine
                }
                Err(_) => HandlerDisposition::Disconnected(h_index),
            },
        }
    }
}

// the send side of a channel, where the message is a result with a possible database error.
pub type ResultChan<T> = cb::Sender<DbQueryResult<T>>;

/// Trait for handlers that can be added to DbThread. the key properties are:
/// * it is recognized in MessageHandler.
/// * it carries the receiver side of a channel
/// * it has a method for processing each message on the channel using a reference to Db.
pub trait Handler {
    type Message;

    fn to_handler(self) -> MessageHandler;

    fn receiver(&self) -> &cb::Receiver<Self::Message>;

    fn handle(&self, msg: Self::Message, db: &Db);
}

pub trait MSender {
    type Message;

    const MSG_NAME: &'static str;

    fn sender(&self) -> &cb::Sender<Self::Message>;

    fn send(&self, msg: Self::Message) {
        self.sender()
            .send(msg)
            //todo: error handling
            .expect(&format!(
                "could not send {}. db thread must have shut down early!",
                Self::MSG_NAME
            ));
    }

    fn send_and_wait<T, F>(&self, f: F) -> T
    where
        F: FnOnce(cb::Sender<T>) -> Self::Message,
    {
        let (s, r) = pass_back();
        let msg = f(s);
        self.send(msg);
        r.recv().expect(&format!(
            "db thread disconnected before responding to {}!",
            Self::MSG_NAME
        ))
    }
}

/// pass_back creates a capacity one channel because the db thread should not block on sending a
/// reply; only the thread waiting for the reply needs to block.
pub fn pass_back<T>() -> (cb::Sender<T>, cb::Receiver<T>) {
    cb::bounded(1)
}
