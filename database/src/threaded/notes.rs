use std::collections::HashSet;

use crossbeam_channel as cb;

use crate::connect_result::DbQueryResult;
use crate::file_index::FileIndexKey;
use crate::notes::model::UpdateNoteData;
use crate::notes::NoteFileData;
use crate::notes::NoteKey;
use crate::tags::SetTagCounts;
use crate::tags::TagName;
use crate::Db;

use super::Handler;
use super::MSender;
use super::MessageHandler;
use super::ResultChan;

/// Creates a channel and wraps up each side of it for use with DbThread.
pub fn note_ops_channel(limit: usize) -> (NoteOpsSender, NoteOpsHandler) {
    let (sender, receiver) = cb::bounded(limit);

    let sender = NoteOpsSender { sender };

    let handler = NoteOpsHandler { receiver };

    (sender, handler)
}

#[derive(Clone)]
pub struct NoteOpsSender {
    sender: cb::Sender<NoteOpsMessage>,
}

/// Ops needed for note indexing.
pub enum NoteOpsMessage {
    AddNoteForFile(NoteFileData, ResultChan<NoteKey>),

    /// message contains file id for note, data to put in note, and channel for expected note key response
    UpdateNoteDataByMDFile(FileIndexKey, UpdateNoteData, ResultChan<NoteKey>),

    ///
    SetTagsOnNote(NoteKey, HashSet<TagName>, ResultChan<SetTagCounts>),
}

impl MSender for NoteOpsSender {
    type Message = NoteOpsMessage;
    const MSG_NAME: &'static str = "note ops request";

    fn sender(&self) -> &cb::Sender<Self::Message> {
        &self.sender
    }
}

impl NoteOpsSender {
    pub fn add_note_for_file(&self, data: NoteFileData) -> DbQueryResult<NoteKey> {
        self.send_and_wait(|c| NoteOpsMessage::AddNoteForFile(data, c))
    }

    pub fn update_note_data_by_md_file(
        &self,
        k: FileIndexKey,
        update: UpdateNoteData,
    ) -> DbQueryResult<NoteKey> {
        self.send_and_wait(|c| NoteOpsMessage::UpdateNoteDataByMDFile(k, update, c))
    }

    pub fn set_tags_on_note(
        &self,
        k: NoteKey,
        tags: HashSet<TagName>,
    ) -> DbQueryResult<SetTagCounts> {
        self.send_and_wait(|c| NoteOpsMessage::SetTagsOnNote(k, tags, c))
    }
}

pub struct NoteOpsHandler {
    receiver: cb::Receiver<NoteOpsMessage>,
}

impl Handler for NoteOpsHandler {
    type Message = NoteOpsMessage;

    fn to_handler(self) -> MessageHandler {
        MessageHandler::NoteOps(self)
    }

    fn receiver(&self) -> &cb::Receiver<Self::Message> {
        &self.receiver
    }

    fn handle(&self, msg: Self::Message, db: &Db) {
        match msg {
            NoteOpsMessage::AddNoteForFile(data, res) => {
                res.send(db.note_ops().add_note_for_file(data))
                    .expect("add_note_for_file - caller was disconnected before response was sent");
            }
            NoteOpsMessage::UpdateNoteDataByMDFile(k, update, res) => {
                let db_res = db
                    .note_ops()
                    .update_note_data_by_md_file(k, update)
                    .and_then(|_| db.note_ops().get_note_by_md_file(k))
                    .map(|e| e.id);
                res.send(db_res).expect(
                    "update_note_for_file - caller was disconnected before response was sent",
                );
            }
            NoteOpsMessage::SetTagsOnNote(file_key, tags, res) => {
                let count_res = db.tag_ops().set_note_tags(file_key, tags);
                res.send(count_res)
                    .expect("set_tags_on_note - caller was disconnected before response was sent");
            }
        }
    }
}
