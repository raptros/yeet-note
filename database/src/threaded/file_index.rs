use std::path::PathBuf;

use crossbeam_channel as cb;
use log::error;

use directory_scan::file_index::FileIndex;
use directory_scan::fs;

use crate::connect_result::DbQueryError;
use crate::file_index::models::DBIndexEntry;
use crate::file_index::FileIndexKey;
use crate::Db;

use super::Handler;
use super::MSender;
use super::MessageHandler;
use super::ResultChan;

/// Creates a channel and wraps up each side of it for use with DbThread.
pub fn file_index_channel(limit: usize) -> (FileIndexSender, FileIndexHandler) {
    let (sender, receiver) = cb::bounded(limit);

    let sender = FileIndexSender { sender };

    let handler = FileIndexHandler { receiver };

    (sender, handler)
}

/// The send side of the file index channel.
///
/// the messages supported by the channel correspond to the interface of `FileIndex`; this struct
/// is intended to be used as an implementation of `FileIndex` once there's a `DbThread` running.
///
/// note that the implementation of the `FileIndex` methods will panic if
/// * they can't send the message - this would mean that the channel is disconnected, implying that
///   the db thread has shut down.
/// * if they expect a response, but get an error trying to receive on the response channel. this
///   would mean that the send side of the response channel has disconnected, again implying that
///   the db thread has died.
#[derive(Clone)]
pub struct FileIndexSender {
    sender: cb::Sender<FileIndexMessage>,
}

pub enum FileIndexMessage {
    GetBase(ResultChan<DBIndexEntry>),

    UpdateIndexTime(bool),

    ListDir(FileIndexKey, ResultChan<Vec<DBIndexEntry>>),

    //no back-passing on this one!
    Delete(FileIndexKey),

    Create(FileIndexKey, fs::Entry, ResultChan<FileIndexKey>),

    //update does not need to stick around for a result either
    Update(FileIndexKey, fs::Entry),

    GetPath(FileIndexKey, ResultChan<PathBuf>),
}

impl MSender for FileIndexSender {
    type Message = FileIndexMessage;
    const MSG_NAME: &'static str = "file index request";

    fn sender(&self) -> &cb::Sender<Self::Message> {
        &self.sender
    }
}

impl FileIndex for FileIndexSender {
    type ID = FileIndexKey;
    type Entry = DBIndexEntry;
    type Error = DbQueryError;

    fn get_base(&self) -> Result<Self::Entry, Self::Error> {
        self.send_and_wait(FileIndexMessage::GetBase)
    }

    fn update_index_time(&self, updates_occurred: bool) -> Result<(), Self::Error> {
        self.send(FileIndexMessage::UpdateIndexTime(updates_occurred));
        Ok(())
    }

    fn list_dir(&self, id: Self::ID) -> Result<Vec<Self::Entry>, Self::Error> {
        self.send_and_wait(move |s| FileIndexMessage::ListDir(id, s))
    }

    fn delete(&self, id: Self::ID) -> Result<(), Self::Error> {
        // let the DB thread deal with the DB error on delete.
        self.send(FileIndexMessage::Delete(id));
        Ok(())
    }

    fn create(&self, parent_id: Self::ID, entry: &fs::Entry) -> Result<Self::ID, Self::Error> {
        //todo: is there some way i can avoid cloning here?
        self.send_and_wait(move |s| FileIndexMessage::Create(parent_id, entry.clone(), s))
    }

    fn update(&self, id: Self::ID, entry: &fs::Entry) -> Result<(), Self::Error> {
        //todo: is there some way i can avoid cloning here?
        self.send(FileIndexMessage::Update(id, entry.clone()));
        Ok(())
    }

    fn get_path(&self, id: Self::ID) -> Result<PathBuf, Self::Error> {
        self.send_and_wait(move |s| FileIndexMessage::GetPath(id, s))
    }
}

/// The receiver side of the file index channel. handles the messages by making the appropriate
/// database calls (via `FileIndexTable`). pass it into a DbThread as a handler.
///
/// some behavior notes
/// * for Delete and Update messages, it currently just logs errors if the operations fail.
/// * for the other operations, it panics if sending the reply message fails - this seems reasonable
///   because the only way that happens is if the reply channel has already become disconnected,
///   which would imply that the other side of the channel has disappeared.
pub struct FileIndexHandler {
    receiver: cb::Receiver<FileIndexMessage>,
}

impl Handler for FileIndexHandler {
    type Message = FileIndexMessage;

    fn to_handler(self) -> MessageHandler {
        MessageHandler::FileIndex(self)
    }

    fn receiver(&self) -> &cb::Receiver<Self::Message> {
        &self.receiver
    }

    fn handle(&self, msg: Self::Message, db: &Db) {
        let file_index = db.file_index();
        match msg {
            FileIndexMessage::GetBase(res) => {
                res.send(file_index.get_base())
                    .expect("get_base - caller disconnected before response was sent");
            }
            FileIndexMessage::UpdateIndexTime(updates_occurred) => {
                if let Err(e) = file_index.update_root_time(updates_occurred) {
                    error!("update index time failed: {:?}", e);
                }
            }
            FileIndexMessage::ListDir(e_id, res) => {
                res.send(file_index.list_dir(e_id))
                    .expect("list_dir - caller disconnected before response was sent");
            }
            FileIndexMessage::Delete(e_id) => {
                if let Err(e) = file_index.delete(e_id) {
                    error!("delete failed: {:?}", e);
                }
            }
            FileIndexMessage::Create(parent_id, entry, res) => {
                res.send(file_index.create(parent_id, &entry))
                    .expect("create - caller disconnected before response was sent");
            }
            FileIndexMessage::Update(e_id, entry) => {
                if let Err(e) = file_index.update(e_id, &entry) {
                    error!("update failed: {:?}", e);
                }
            }
            FileIndexMessage::GetPath(e_id, res) => {
                res.send(file_index.get_path(e_id))
                    .expect("get_path - caller disconnected before response was sent");
            }
        }
    }
}
