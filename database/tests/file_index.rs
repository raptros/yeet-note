use std::io::Write;
use std::path::Path;

use failure::bail;
use failure::format_err;
use failure::Fallible;
use failure::ResultExt;
use tempfile::TempDir;

use database::file_index::FileIndexKey;
use directory_scan::fs;
use directory_scan::result::DeletedEntry;
use directory_scan::FileIndexEvent;
use directory_scan::IndexedFile;
use directory_scan::ScanResultEntry;
use directory_scan::ScanWithIndex;
use fs_tree_creator::fs_tree;
use fs_tree_creator::FSTree;

use crate::common::TempDb;

mod common;

macro_rules! expect_none_next {
    ($e: ident) => {
        assert!($e.next().is_none(), "expected end of iterator")
    };
}

macro_rules! time_assert {
    (prev $prev:expr, unchanged $next:expr, $msg:expr) => {
        assert!(
            $prev.unchanged(&$next),
            "after {}, expected no change but had prev={:?} and next={:?}",
            $msg,
            $prev,
            $next
        );
    };
    (prev $prev:expr, reindexed $next:expr, $msg:expr) => {
        assert!(
            $prev.reindexed(&$next),
            "after {}, expected reindex and no updated, but had prev={:?} and next={:?}",
            $msg,
            $prev,
            $next
        );
    };
    (prev $prev:expr, unchanged or reindexed $next:expr, $msg:expr) => {
        assert!(
            $prev.reindexed(&$next) || $prev.unchanged(&$next),
            "after {}, expected no update but possible reindex, but had prev={:?} and next={:?}",
            $msg,
            $prev,
            $next
        );
    };
    (prev $prev:expr, updated $next:expr, $msg:expr) => {
        assert!(
            $prev.updated(&$next),
            "after {}, expected update but had prev={:?} and next={:?}",
            $msg,
            $prev,
            $next
        );
    };
}

macro_rules! expect_next_success {
    (@next $i:ident, $position:expr) => {{
        let next = match $i.next() {
            None => bail!("missing an entry @\"{}\"", $position),
            Some(v) => v,
        };
        next.context(format_err!("expected next entry to be a success @\"{}\"", $position))
    }};

    (@check_var $i: ident, $variant: ident, $position: expr, $msg: expr) => {{
        let next_here = expect_next_success!(@next $i, $position)?;
        match next_here {
            FileIndexEvent::$variant(capture) => capture,
            o => bail!("expected event @\"{}\" to be {}: {}. instead got {:?}", $position, stringify!($variant), $msg, o),
        }
    }};

    (created $i: ident, $position:expr, $msg: expr) => {
        expect_next_success!(@check_var $i, Created, $position, $msg)
    };

    (updated $i: ident, $position:expr, $msg: expr) => {
        expect_next_success!(@check_var $i, Updated, $position, $msg)
    };
    (unchanged $i: ident, $position:expr, $msg: expr) => {
        expect_next_success!(@check_var $i, Unchanged, $position, $msg)
    };
    (deleted $i: ident, $position:expr, $msg: expr) => {
        expect_next_success!(@check_var $i, Deleted, $position, $msg)
    };
}

#[test]
fn single_file_test() -> Fallible<()> {
    let fs_tree = fs_tree![
        file "hello.txt" => "this is a test file";
    ];

    let ops = ScanOps::create(fs_tree)?;

    let time_0 = ops.get_index_times()?;

    //wait here so that modified time is distinctly earlier than last index time
    //this means that when we come back to do the second scan, the last modify time on the actual file
    //will still be earlier than the time of the first indexing
    wait_a_second();

    /* scan number 1: we expect Created **/
    let mut res_iter = ops.do_scan(1)?.into_iter();

    let res: IndexedFile<FileIndexKey> = expect_next_success!(created res_iter, "first scan first entry", "first time the entry has been seen");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    //now that the scan iterator is fully consumed, check the times.
    let time_1 = ops.get_index_times()?;
    // updated - a second has passed by, a change has occurred.
    time_assert!(prev time_0, updated time_1, "scan 1");

    /* scan number 2: we expect Unchanged **/
    let mut res_iter = ops.do_scan(1)?.into_iter();

    let res: IndexedFile<FileIndexKey> =
        expect_next_success!(unchanged res_iter, "second scan first entry", "entry had no changes");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    //now that the scan iterator is fully consumed, check the times.
    let time_2 = ops.get_index_times()?;
    //no time has passed, no changes have occurred
    time_assert!(prev time_1, unchanged or reindexed time_2, "scan 2");

    /* scan number 3: we append to the file and expect it to be updated*/

    //don't wait, just append. this should mark it as changed
    ops.touch_file("hello.txt", "here is more stuff")?;

    //now redo the scan without waiting. because the length changed, this should show it as updated
    let mut res_iter = ops.do_scan(1)?.into_iter();

    let res: IndexedFile<FileIndexKey> =
        expect_next_success!(updated res_iter, "third scan first entry", "entry was appended to");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    //now that the scan iterator is fully consumed, check the times.
    let time_3 = ops.get_index_times()?;
    //even though a change has occurred, not enough time has passed
    time_assert!(prev time_2, unchanged time_3, "scan 3");

    /* scan number 4: we wait a second, and expect it to still show as updated  **/
    // we expect to see status Updated again because the last index time in the DB,
    // which was set during the previous scan, was set less than a second after the file was modified.
    // this verifies our intentional treatment of "index time == mod time" as indicating an update
    //
    // we wait a second here so that the scan *after* this one will see the entry as unchanged
    wait_a_second();
    let mut res_iter = ops.do_scan(1)?.into_iter();

    let res: IndexedFile<FileIndexKey> = expect_next_success!(updated res_iter, "fourth scan first entry", "last index time in db less than a second after modification");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    //now that the scan iterator is fully consumed, check the times.
    let time_4 = ops.get_index_times()?;
    //enough time has passed, and a change has occurred
    time_assert!(prev time_3, updated time_4, "scan 4");

    /* scan number 5: we expect the file to show as unchanged **/
    //the previous scan updated the last_index_time to be distinctly after the file's mod time
    let mut res_iter = ops.do_scan(1)?.into_iter();

    let res: IndexedFile<FileIndexKey> = expect_next_success!(unchanged res_iter, "fifth scan first entry", "last index time in db more than a second after modification");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    //now that the scan iterator is fully consumed, check the times.
    let time_5 = ops.get_index_times()?;
    time_assert!(prev time_4, unchanged time_5, "scan 5");

    /* scan number 6: delete the file and expect to see it marked as deleted! */
    ops.delete_file("hello.txt")?;

    wait_a_second();

    let mut res_iter = ops.do_scan(1)?.into_iter();

    let res: DeletedEntry<FileIndexKey> =
        expect_next_success!(deleted res_iter, "sixth scan first entry", "deletion");

    assert_eq!(res.name(), "hello.txt");
    assert_eq!(*res.entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    //now that the scan iterator is fully consumed, check the times.
    let time_6 = ops.get_index_times()?;
    time_assert!(prev time_5, updated time_6, "scan 6");

    Ok(())
}

#[test]
fn single_file_create_wait_unchanged() -> Fallible<()> {
    let fs_tree = fs_tree![
        file "hello.txt" => "this is a test file";
    ];

    let ops = ScanOps::create(fs_tree)?;

    let time_0 = ops.get_index_times()?;

    //wait here so that modified time is distinctly earlier than last index time
    //this means that when we come back to do the second scan, the last modify time on the actual file
    //will still be earlier than the time of the first indexing
    wait_a_second();

    /* scan number 1: we expect Created **/
    let mut res_iter = ops.do_scan(1)?.into_iter();

    let res: IndexedFile<FileIndexKey> = expect_next_success!(created res_iter, "first scan first entry", "first time the entry has been seen");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    //now that the scan iterator is fully consumed, check the times.
    let time_1 = ops.get_index_times()?;
    // updated - a second has passed by, a change has occurred.
    time_assert!(prev time_0, updated time_1, "scan 1");

    wait_a_second();

    /* scan number 2: we expect Unchanged **/
    let mut res_iter = ops.do_scan(1)?.into_iter();

    let res: IndexedFile<FileIndexKey> =
        expect_next_success!(unchanged res_iter, "second scan first entry", "entry had no changes");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    //now that the scan iterator is fully consumed, check the times.
    let time_2 = ops.get_index_times()?;
    //some time has passed, but no changes have occurred
    time_assert!(prev time_1, reindexed time_2, "scan 2");

    Ok(())
}

#[test]
fn single_file_in_single_dir_test() -> Fallible<()> {
    let fs_tree = fs_tree![
        dir "test" => [
            file "hello.txt" => "this is a test file";
        ];
    ];

    let ops = ScanOps::create(fs_tree)?;

    //wait here so that modified time is distinctly earlier than last index time
    //this means that when we come back to do the second scan, the last modify time on the actual file
    //will still be earlier than the time of the first indexing
    wait_a_second();

    /* scan number 1: we expect Created **/
    let mut res_iter = ops.do_scan(2)?.into_iter();

    let res: IndexedFile<FileIndexKey> = expect_next_success!(created res_iter, "first scan first entry", "first time the entry has been seen");

    assert_eq!(res.entry().name(), "test");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::Directory);

    let res: IndexedFile<FileIndexKey> = expect_next_success!(created res_iter, "first scan second entry", "first time the entry has been seen");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    /* scan number 2: we expect Unchanged **/
    let mut res_iter = ops.do_scan(2)?.into_iter();

    let res: IndexedFile<FileIndexKey> = expect_next_success!(unchanged res_iter, "second scan first entry", "directory had no changes");

    assert_eq!(res.entry().name(), "test");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::Directory);

    let res: IndexedFile<FileIndexKey> =
        expect_next_success!(unchanged res_iter, "second scan second entry", "file had no changes");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    /* scan number 3: we append to the file and expect it to be updated*/

    //don't wait, just append. this should mark it as changed
    ops.touch_file("test/hello.txt", "here is more stuff")?;

    //now redo the scan without waiting. because the length changed, this should show it as updated
    let mut res_iter = ops.do_scan(2)?.into_iter();

    let res: IndexedFile<FileIndexKey> = expect_next_success!(unchanged res_iter, "third scan first entry", "directory had no changes");

    assert_eq!(res.entry().name(), "test");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::Directory);

    let res: IndexedFile<FileIndexKey> =
        expect_next_success!(updated res_iter, "third scan second entry", "file was written");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    /* scan number 4: we wait a second, and expect it to still show as updated  **/
    // we expect to see status Updated again because the last index time in the DB,
    // which was set during the previous scan, was set less than a second after the file was modified.
    // this verifies our intentional treatment of "index time == mod time" as indicating an update
    //
    // we wait a second here so that the scan *after* this one will see the entry as unchanged
    wait_a_second();

    let mut res_iter = ops.do_scan(2)?.into_iter();

    let res: IndexedFile<FileIndexKey> = expect_next_success!(unchanged res_iter, "fourth scan first entry", "directory had no changes");

    assert_eq!(res.entry().name(), "test");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::Directory);

    let res: IndexedFile<FileIndexKey> = expect_next_success!(updated res_iter, "fourth scan second entry", "file update still shows");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    /* scan number 5: we expect the file to show as unchanged **/
    //the previous scan updated the last_index_time to be distinctly after the file's mod time
    let mut res_iter = ops.do_scan(2)?.into_iter();

    let res: IndexedFile<FileIndexKey> = expect_next_success!(unchanged res_iter, "fifth scan first entry", "directory had no changes");

    assert_eq!(res.entry().name(), "test");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::Directory);

    let res: IndexedFile<FileIndexKey> = expect_next_success!(unchanged res_iter, "fifth scan second entry", "file update now indexed");

    assert_eq!(res.entry().name(), "hello.txt");
    assert_eq!(*res.entry().entry_type(), fs::EntryType::File);

    expect_none_next!(res_iter);

    /* scan number 6: delete the directory and expect to see it marked as deleted! */
    ops.delete_dir("test")?;

    let mut res_iter = ops.do_scan(1)?.into_iter();

    let res: DeletedEntry<FileIndexKey> =
        expect_next_success!(deleted res_iter, "sixth scan first entry", "deletion");

    assert_eq!(res.name(), "test");
    assert_eq!(*res.entry_type(), fs::EntryType::Directory);

    expect_none_next!(res_iter);

    Ok(())
}

fn wait_a_second() {
    std::thread::sleep(std::time::Duration::from_secs(1));
}

struct ScanOps {
    temp_db: TempDb,
    scan_dir: TempDir,
}

impl ScanOps {
    fn create(fs_tree: FSTree) -> Fallible<Self> {
        let temp_db = TempDb::connect()?;
        let scan_dir = fs_tree.temp_dir()?;

        Ok(ScanOps { temp_db, scan_dir })
    }

    fn get_index_times(&self) -> Fallible<IndexTimes> {
        let root = self
            .temp_db
            .db()
            .file_index()
            .get_by_id(FileIndexKey::ROOT)
            .context("failed to get root for index times")?;

        Ok(IndexTimes {
            last_index_time: root.last_index_time,
            md_modified: root.md_modified,
        })
    }

    fn touch_file<P: AsRef<Path>>(&self, rel_path: P, add_contents: &str) -> Fallible<()> {
        let target_path = self.scan_dir.path().join(rel_path.as_ref());

        let mut f = std::fs::OpenOptions::new()
            .append(true)
            .open(target_path)
            .context(format_err!(
                "failed to open file {}",
                rel_path.as_ref().display()
            ))?;

        f.write_all(add_contents.as_bytes())
            .context("failed to write!")?;

        Ok(())
    }

    fn delete_file<P: AsRef<Path>>(&self, rel_path: P) -> Fallible<()> {
        let target_path = self.scan_dir.path().join(rel_path.as_ref());
        std::fs::remove_file(target_path)?;
        Ok(())
    }

    fn delete_dir<P: AsRef<Path>>(&self, rel_path: P) -> Fallible<()> {
        let target_path = self.scan_dir.path().join(rel_path.as_ref());
        std::fs::remove_dir_all(target_path).context(format_err!(
            "deleting directory {}",
            rel_path.as_ref().display()
        ))?;
        Ok(())
    }

    fn do_scan(&self, expect_len: usize) -> Fallible<Vec<ScanResultEntry<FileIndexKey>>> {
        let scan_with_index =
            ScanWithIndex::new(self.scan_dir.path(), self.temp_db.db().file_index());

        let results: Vec<ScanResultEntry<FileIndexKey>> =
            scan_with_index.prepare()?.into_iter().collect();

        assert_eq!(
            results.len(),
            expect_len,
            "got {} entries in result, expected {}",
            results.len(),
            expect_len
        );

        Ok(results)
    }
}

#[derive(Debug)]
struct IndexTimes {
    last_index_time: i64,
    md_modified: i64,
}

impl IndexTimes {
    fn unchanged(&self, next: &IndexTimes) -> bool {
        self.last_index_time == next.last_index_time && self.md_modified == next.md_modified
    }

    fn reindexed(&self, next: &IndexTimes) -> bool {
        self.last_index_time < next.last_index_time && self.md_modified == next.md_modified
    }

    fn updated(&self, next: &IndexTimes) -> bool {
        self.last_index_time < next.last_index_time && self.md_modified < next.md_modified
    }
}
