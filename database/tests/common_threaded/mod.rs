use std::thread::JoinHandle;

use failure::format_err;
use failure::Fallible;

use database::threaded::DbThread;
use database::threaded::Handler;
use database::ConnectStr;
use database::Db;

pub struct TempDbThreadBuilder {
    db_thread_builder: DbThread,
    /// this is kept as a field simply because as soon as it gets dropped the TempPath underneath it removes the file.
    temp_file: tempfile::NamedTempFile,
}

impl TempDbThreadBuilder {
    pub fn connect() -> Fallible<Self> {
        let temp_file = tempfile::NamedTempFile::new()?;
        let db = Db::connect(&ConnectStr::Path(temp_file.path()))?;

        let db_thread_builder = DbThread::new(db);

        Ok(TempDbThreadBuilder {
            db_thread_builder,
            temp_file,
        })
    }

    pub fn add_handler<H: Handler>(mut self, h: H) -> Self {
        self.db_thread_builder = self.db_thread_builder.add_handler(h);
        self
    }

    pub fn run(self) -> TempDbThread {
        let temp_file = self.temp_file;
        let join_handle = self.db_thread_builder.run();
        TempDbThread {
            join_handle,
            _temp_file: temp_file,
        }
    }
}

pub struct TempDbThread {
    join_handle: JoinHandle<Db>,

    /// this is kept as a field simply because as soon as it gets dropped the TempPath underneath it removes the file.
    _temp_file: tempfile::NamedTempFile,
}

impl TempDbThread {
    pub fn join(self) -> Fallible<Db> {
        self.join_handle
            .join()
            .map_err(|_| format_err!("failed to join db thread"))
    }
}
