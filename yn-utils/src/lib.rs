//! utilities and macros
use std::collections::VecDeque;

#[macro_export]
macro_rules! ret_none {
    ($e:expr) => {
        match $e {
            Some(v) => v,
            None => return None,
        }
    };
}

#[macro_export]
macro_rules! try_opt {
    ($e:expr) => {
        match $e {
            Ok(v) => v,
            Err(e) => return Some(Err(e.into())),
        }
    };
}

/// Incredibly, this works. However, intellij is then completely bewildered.
#[macro_export]
macro_rules! error_with_kind {
    ($error_name: ident, $kind_name: ident) => {
        #[derive(Debug)]
        pub struct $error_name {
            inner: Context<$kind_name>,
        }

        impl failure::Fail for $error_name {
            fn cause(&self) -> Option<&failure::Fail> {
                self.inner.cause()
            }

            fn backtrace(&self) -> Option<&failure::Backtrace> {
                self.inner.backtrace()
            }
        }

        impl std::fmt::Display for $error_name {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                std::fmt::Display::fmt(&self.inner, f)
            }
        }

        impl $error_name {
            pub fn kind(&self) -> $kind_name {
                *self.inner.get_context()
            }
        }

        impl From<$kind_name> for $error_name {
            fn from(kind: $kind_name) -> Self {
                $error_name {
                    inner: Context::new(kind),
                }
            }
        }

        impl From<Context<$kind_name>> for $error_name {
            fn from(inner: Context<$kind_name>) -> Self {
                $error_name { inner }
            }
        }
    };
}

pub trait ResultConvert<T, E> {
    fn from_err<E2>(self) -> Result<T, E2>
    where
        E2: From<E>;

    fn void_res(self) -> Result<(), E>;
}

impl<T, E> ResultConvert<T, E> for Result<T, E> {
    fn from_err<E2>(self) -> Result<T, E2>
    where
        E2: From<E>,
    {
        self.map_err(From::from)
    }

    fn void_res(self) -> Result<(), E> {
        self.map(|_| {})
    }
}

/// Wraps a vec to present a stack-specific interface
pub struct Stack<T>(Vec<T>);

impl<T> Stack<T> {
    pub fn new(first: T) -> Self {
        Stack(vec![first])
    }

    pub fn push(&mut self, item: T) {
        self.0.push(item)
    }

    pub fn peek(&self) -> Option<&T> {
        self.0.last()
    }

    pub fn peek_mut(&mut self) -> Option<&mut T> {
        self.0.last_mut()
    }

    pub fn pop(&mut self) -> Option<T> {
        self.0.pop()
    }
}

/// Wraps a VecDeque to present a queue-specific interface.
pub struct Queue<T>(VecDeque<T>);

impl<T> Queue<T> {
    pub fn new() -> Self {
        Queue(VecDeque::new())
    }

    pub fn add(&mut self, item: T) {
        self.0.push_back(item)
    }

    pub fn peek(&self) -> Option<&T> {
        self.0.front()
    }

    pub fn peek_mut(&mut self) -> Option<&mut T> {
        self.0.front_mut()
    }

    pub fn pull(&mut self) -> Option<T> {
        self.0.pop_front()
    }
}
