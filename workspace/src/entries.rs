use std::path::PathBuf;

use database::file_index::FileIndexKey;
use directory_scan::fs;
use directory_scan::result::DeletedEntry;
use directory_scan::result::FileIndexEvent;
use directory_scan::result::IndexedFile;

#[derive(Debug)]
pub struct WorkspaceEntry {
    pub id: FileIndexKey,
    pub parent_id: FileIndexKey,
    pub entry_type: fs::EntryType,
    pub path: PathBuf,
}

impl WorkspaceEntry {
    pub fn for_indexed_entry(indexed: IndexedFile<FileIndexKey>) -> Option<Self> {
        let id = indexed.id();
        let parent_id = indexed.parent_id();

        let (path, entry_type) = indexed.into_entry().into_path_and_type();

        match entry_type {
            fs::EntryType::Directory | fs::EntryType::File => {
                let entry = WorkspaceEntry {
                    id,
                    parent_id,
                    entry_type,
                    path,
                };
                Some(entry)
            }
            _ => None,
        }
    }
}

#[derive(Debug)]
pub struct DeletedWorkspaceEntry {
    pub id: FileIndexKey,
    pub parent_id: FileIndexKey,
    //we don't care about the path for this deleted entry:
    // it's deleted, so it obviously doesn't exist in the FS anymore.
    // there's nothing to go look at
}

impl DeletedWorkspaceEntry {
    pub fn for_deleted_entry(deleted: &DeletedEntry<FileIndexKey>) -> Option<Self> {
        if deleted.entry_type().is_file() {
            let entry = DeletedWorkspaceEntry {
                id: deleted.id(),
                parent_id: deleted.parent_id(),
            };
            Some(entry)
        } else {
            None
        }
    }
}

#[derive(Debug)]
pub enum WorkspaceEntryEvent {
    Created(WorkspaceEntry),
    Modified(WorkspaceEntry),
    Deleted(DeletedWorkspaceEntry),
}

impl WorkspaceEntryEvent {
    pub fn for_scan_event(event: FileIndexEvent<FileIndexKey>) -> Option<Self> {
        match event {
            FileIndexEvent::Created(indexed) => {
                WorkspaceEntry::for_indexed_entry(indexed).map(WorkspaceEntryEvent::Created)
            }

            FileIndexEvent::Updated(indexed) => {
                //for update events, all the indexer needs to care about are file modifications
                // - directory modifications are basically meaningless
                if indexed.entry().entry_type().is_file() {
                    WorkspaceEntry::for_indexed_entry(indexed).map(WorkspaceEntryEvent::Modified)
                } else {
                    None
                }
            }

            FileIndexEvent::Unchanged(_) => None,

            FileIndexEvent::Deleted(deleted) => {
                DeletedWorkspaceEntry::for_deleted_entry(&deleted).map(WorkspaceEntryEvent::Deleted)
            }
        }
    }
}
