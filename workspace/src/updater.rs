use std::thread;

use crossbeam_channel as cb;
use failure::bail;
use failure::format_err;
use failure::Fallible;
use failure::ResultExt;
use log::info;
use log::trace;

use database::threaded as dbt;
use database::threaded::file_index::FileIndexSender;
use directory_scan::result::FileScanStats;
use directory_scan::ScanWithIndex;

use crate::entries::WorkspaceEntryEvent;
use crate::note::NoteIndexer;
use crate::Workspace;
use crate::WsBasePath;

pub struct Updater {
    db_thread: dbt::DbThread,
    ws_base_path: WsBasePath,
    scanner: ScanWithIndex<FileIndexSender>,
    send_to_indexer: cb::Sender<IndexControl>,
    indexer_thread: IndexerThread,
}

const FILE_INDEX_CHANNEL_BUFFER_SIZE: usize = 100;

const NOTE_INDEX_CHANNEL_SIZE: usize = 100;

impl Updater {
    pub fn for_workspace(ws: Workspace) -> Self {
        //peel open the workspace
        let ws_base_path = ws.base_path;
        let db = ws.db;

        let (file_index_sender, file_index_handler) =
            dbt::file_index::file_index_channel(FILE_INDEX_CHANNEL_BUFFER_SIZE);

        let (note_ops_sender, note_ops_handler) =
            dbt::notes::note_ops_channel(NOTE_INDEX_CHANNEL_SIZE);

        let db_thread = dbt::DbThread::new(db)
            .add_handler(file_index_handler)
            .add_handler(note_ops_handler);

        let scanner =
            directory_scan::ScanWithIndex::new(ws_base_path.0.as_path(), file_index_sender.clone())
                //filter out hidden files, including the .yn/ directory
                .filter_names(|name| !name.starts_with('.'));

        let (send_to_indexer, indexer_events) = cb::bounded(NOTE_INDEX_CHANNEL_SIZE);

        let indexer = NoteIndexer::new(file_index_sender, note_ops_sender);

        let indexer_thread = IndexerThread {
            indexer,
            events: indexer_events,
        };

        Updater {
            db_thread,
            ws_base_path,
            scanner,
            send_to_indexer,
            indexer_thread,
        }
    }

    pub fn perform(self) -> Fallible<Workspace> {
        let mut stats = FileScanStats::new();

        //start up the db thread. this *must* happen before trying to prepare the scanner!
        let db_handle = self.db_thread.run();

        let index_join_handle = self.indexer_thread.run();

        let scan = self
            .scanner
            .prepare()
            .context(format_err!("failure during scan preparation"))?;

        for entry_res in scan.into_iter() {
            let next_ev = match entry_res {
                Ok(entry) => entry,
                Err(error) => {
                    super::log_error(error);
                    continue;
                }
            };

            stats.tick_event(&next_ev);

            if let Some(update) = WorkspaceEntryEvent::for_scan_event(next_ev) {
                self.send_to_indexer
                    .send(IndexControl::Event(update))
                    .context("indexer thread died!")?;
            }
        }

        //todo: improve post-update error processing

        self.send_to_indexer
            .send(IndexControl::Stop)
            .context("indexer thread died before stop could be sent!")?;

        if index_join_handle.join().is_err() {
            bail!("index thread had a panic");
        }

        let db = db_handle
            .join()
            .map_err(|_| format_err!("db thread must have failed, no db available afterwards"))?;

        info!("stats are: {}", stats);

        Ok(Workspace::new(self.ws_base_path, db))
    }
}

enum IndexControl {
    Event(WorkspaceEntryEvent),
    Stop,
}

struct IndexerThread {
    indexer: NoteIndexer,
    events: cb::Receiver<IndexControl>,
}

impl IndexerThread {
    pub fn run(self) -> thread::JoinHandle<()> {
        thread::spawn(move || {
            for event in self.events {
                let ev = match event {
                    IndexControl::Stop => break,
                    IndexControl::Event(e) => e,
                };

                if let Err(error) = self.indexer.handle(ev) {
                    //for now, just log any errors
                    super::log_error(error);
                }
            }
            trace!("indexer thread: shutting down");
        })
    }
}
