use failure::format_err;
use failure::Fallible;
use failure::ResultExt;
use log::trace;

use database::notes::model::UpdateNoteData;
use database::notes::NoteEntryType;
use database::notes::NoteFileData;
use database::tags::TagName;
use database::threaded::file_index::FileIndexSender;
use database::threaded::notes::NoteOpsSender;
use directory_scan::file_index::FileIndex;

use crate::entries::WorkspaceEntryEvent;

use self::note_path::*;

pub mod file_data;
pub mod note_path;

pub struct NoteIndexer {
    file_index_sender: FileIndexSender,
    note_ops_sender: NoteOpsSender,
}

impl NoteIndexer {
    pub fn new(file_index_sender: FileIndexSender, note_ops_sender: NoteOpsSender) -> Self {
        NoteIndexer {
            file_index_sender,
            note_ops_sender,
        }
    }

    pub fn handle(&self, event: WorkspaceEntryEvent) -> Fallible<()> {
        match event {
            WorkspaceEntryEvent::Created(entry) => {
                let parsed_path = NotePath::parse(entry.path, entry.entry_type.is_dir());

                if entry.entry_type.is_dir() {
                    let note_data = NoteFileData {
                        name: parsed_path.name.name,
                        date: parsed_path.path_date,
                        parent_dir_id: entry.parent_id,
                        file_index_id: entry.id,
                        entry_type: NoteEntryType::Directory,
                    };

                    //let indexer thread wrapper log any errors that come out of this, for now
                    let response = self.note_ops_sender.add_note_for_file(note_data)?;
                    trace!("added new directory note: {:?}", response);
                } else if entry.entry_type.is_file()
                    && parsed_path.name.extension == Extension::Markdown
                {
                    let file_data = self.read_metadata_from_file(parsed_path)?;

                    let note_data = NoteFileData {
                        name: file_data.path.name.name,
                        date: file_data.date,
                        parent_dir_id: entry.parent_id,
                        file_index_id: entry.id,
                        entry_type: NoteEntryType::Markdown,
                    };

                    let tags = file_data.tags.into_iter().map(TagName::from).collect();

                    //let indexer thread wrapper log any errors that come out of this, for now
                    let note_key = self.note_ops_sender.add_note_for_file(note_data)?;
                    let _counts = self.note_ops_sender.set_tags_on_note(note_key, tags)?;

                    //todo
                    trace!("added new markdown note");
                }
            }
            WorkspaceEntryEvent::Modified(entry) => {
                let parsed_path = NotePath::parse(entry.path, entry.entry_type.is_dir());
                if entry.entry_type.is_file() && parsed_path.name.extension == Extension::Markdown {
                    let file_data = self.read_metadata_from_file(parsed_path)?;

                    let note_data = UpdateNoteData {
                        note_date: file_data.date,
                    };

                    let tags = file_data.tags.into_iter().map(TagName::from).collect();

                    //let indexer thread wrapper log any errors that come out of this, for now
                    let note_key = self
                        .note_ops_sender
                        .update_note_data_by_md_file(entry.id, note_data)?;

                    let _counts = self.note_ops_sender.set_tags_on_note(note_key, tags)?;
                    trace!("updated existing markdown note");
                }
            }
            WorkspaceEntryEvent::Deleted(deleted) => {
                //at this point, the deletion should have cascaded fully.
                //the only thing to do, if at all, would be to re-index any parent notes
                //it would would need the path to do that though.
                //or it could use the parent id, which might be a suitable thing to pass in. let's do that.
                let parent_path =
                    self.file_index_sender
                        .get_path(deleted.parent_id)
                        .context(format_err!(
                            "failed to get path for parent of deleted entry {:?}",
                            deleted
                        ))?;
                trace!(
                    "entry {} deleted from directory {}",
                    deleted.id,
                    parent_path.display()
                );
            }
        };
        Ok(())
    }

    pub fn read_metadata_from_file(&self, path: NotePath) -> Fallible<NoteMetadata> {
        let read_data = file_data::read_from_markdown::<file_data::NoteFileData, _>(&path.full)?;

        let date = path.path_date.or(read_data.date);

        let tags = read_data.tags.tags();

        Ok(NoteMetadata { path, date, tags })
    }
}

#[derive(Debug)]
pub struct NoteMetadata {
    path: NotePath,
    date: Option<chrono::NaiveDate>,
    tags: Vec<String>,
}
