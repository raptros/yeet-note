//! put the structs and enums here for representing note data needed to figure out database operations

use std::borrow::Cow;
use std::path::Path;
use std::path::PathBuf;

use chrono::NaiveDate;

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct NotePath {
    pub full: PathBuf,
    pub path_date: Option<NaiveDate>,
    pub name: NoteName,
    pub is_dir: bool,
}

impl NotePath {
    pub fn parse<P: Into<PathBuf>>(p: P, is_dir: bool) -> Self {
        let full = p.into();

        let name = NoteName::extract(&full, is_dir);

        let path_date = parse_path_date(&full);

        NotePath {
            full,
            path_date,
            name,
            is_dir,
        }
    }
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct NoteName {
    pub name: String,
    pub extension: Extension,
}

impl NoteName {
    pub fn extract<P: AsRef<Path>>(p: P, is_dir: bool) -> Self {
        let path = p.as_ref();

        let name_part = if is_dir {
            path.file_name()
        } else {
            path.file_stem()
        };

        let name = name_part
            .map(|n| n.to_string_lossy().into_owned())
            .unwrap_or("".to_string());

        let extension = match path.extension() {
            None => Extension::Other,
            Some(ref ext) => match &*ext.to_string_lossy() {
                "md" | "markdown" => Extension::Markdown,
                "toml" => Extension::TOML,
                _ => Extension::Other,
            },
        };

        NoteName { name, extension }
    }
}

pub fn parse_path_date(path: &Path) -> Option<NaiveDate> {
    let init_components: Vec<Cow<str>> = path
        .components()
        .take(3)
        .map(|comp| comp.as_os_str().to_string_lossy())
        .collect();

    if init_components.len() < 3 {
        return None;
    }

    let res = parse_components(
        &init_components[0],
        &init_components[1],
        &init_components[2],
    );

    let (year, month, day) = match res {
        Ok(v) => v,
        Err(_) => return None,
    };

    chrono::NaiveDate::from_ymd_opt(year, month, day)
}

fn parse_components(y: &str, m: &str, d: &str) -> Result<(i32, u32, u32), std::num::ParseIntError> {
    Ok((y.parse()?, m.parse()?, d.parse()?))
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub enum Extension {
    Markdown,
    TOML,
    Other,
}

#[cfg(test)]
mod tests {
    use chrono::NaiveDate;

    use super::*;

    macro_rules! gen_tests {
        (@mt $name:ident, $is_dir:expr, $path:expr, $path_date: expr, $f_name:expr, $ext:expr) => {
            #[test]
            fn $name() {
                let parsed = NotePath::parse($path, $is_dir);
                let parts = (parsed.path_date, parsed.name.name.as_str(), parsed.name.extension);
                assert_eq!(parts, ($path_date, $f_name, $ext));
            }
        };
        (@ymd $ymd:expr) => {
            Some(NaiveDate::from_ymd($ymd.0, $ymd.1, $ymd.2))
        };
        () => {};
        ($name:ident => dir $path:expr => no date, name $f_name:expr; $($x:tt)*) => {
            gen_tests!(@mt $name, true, $path, None, $f_name, Extension::Other);
            gen_tests!($($x)*);
        };
        ($name:ident => dir $path:expr => ymd $ymd:expr, name $f_name:expr; $($x:tt)*) => {
            gen_tests!(@mt $name, true, $path, gen_tests!(@ymd $ymd), $f_name, Extension::Other);
            gen_tests!($($x)*);
        };
        ($name:ident => file $path:expr => no date, name $f_name:expr, $ext:expr; $($x:tt)*) => {
            gen_tests!(@mt $name, false, $path, None, $f_name, $ext);
            gen_tests!($($x)*);
        };
        ($name:ident => file $path:expr => ymd $ymd:expr, name $f_name:expr, $ext:expr; $($x:tt)*) => {
            gen_tests!(@mt $name, false, $path, gen_tests!(@ymd $ymd), $f_name, $ext);
            gen_tests!($($x)*);
        };
    }

    gen_tests![
        empty_path => dir "" =>  no date, name "";

        single_markdown => file "note.md" => no date, name "note", Extension::Markdown;

        single_toml => file "note.toml" => no date, name "note", Extension::TOML;

        single_other => file "note.js" => no date, name "note", Extension::Other;

        single_dir => dir "note" => no date, name "note";

        single_dir_with_dotted_name => dir "note.whatever" => no date, name "note.whatever";

        double_dotted_markdown => file "note.whatever.md" => no date, name "note.whatever", Extension::Markdown;

        markdown_in_dir => file "test/note.md" => no date, name "note", Extension::Markdown;

        dir_in_dirs => dir "one/two/three/four" => no date, name "four";

        date_dir_top_level => file "1942/02/15/note.md" => ymd (1942, 02, 15), name "note", Extension::Markdown;

        date_dir_in_dir => file "1942/02/15/test/multiple/note.md" => ymd (1942, 02, 15), name "note", Extension::Markdown;

        non_decimal_day_date_fakeout => file "2012/02/pants/fake.md" => no date, name "fake", Extension::Markdown;

        out_of_range_month_date_fakeout => file "2012/15/22/fake.md" => no date, name "fake", Extension::Markdown;
    ];
}
