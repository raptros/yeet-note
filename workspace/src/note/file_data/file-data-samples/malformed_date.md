malformed date
==============

this file has a malformed date line

yn-date: 2231-123-11

it should be ignored.

todo: maybe add more malformed dates until there's a valid date at the end, which should then be the chosen date...

this one is invalid because it is well formatted but 57 is a bad day of month
yn-date: 2018-02-57


this one is bad because the month is single digit, not 0 padded
yn-date: 2013-2-15

this one is bad bc the day of month is single digit
yn-date: 2013-11-3

this is bad because the year isn't 4 digits
yn-date 747-09-22

this date is bad because i mashed the keyboard instead of writing out a date
yn-date: agfio4n4ganag4nifn4ifn4ifna4iogfna34ifna4winfio4nfia4niton4igna4iagni4gn4intgi4


this date is bad because yeet isn't a real date
yn-date: yeet

this date is bad because 20X6 isn't real yet
yn-date: 20X6-05-17

here is a valid date
yn-date: 2016-05-12