three tags
==========

this file has two groups with of tags tags!
it also has two dates
yn-date: 2018-12-31
tags:
+ one
+ two
+ three
the second date however should be ignored in favor of the first;
this second, fake date is as follows:
2018-05-05

here's the second group
tags:
+ twenty30
+ five_five
+ pants    but the rest of this line should be ignored
* this line should not get picked up a a tag

note that there is whitespace after the second tags, that should be ignored.
all of the tags in both groups should be taken

here's a final fakeout
tags:
since there's no plus, there shouldn't be any tags. but also no problems either