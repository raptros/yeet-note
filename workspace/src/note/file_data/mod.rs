//! Extract data from markdown and toml files
use std::default::Default;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;

use failure::format_err;
use failure::Fallible;
use failure::ResultExt;
use regex::Regex;

#[cfg(test)]
mod tests;

//any data that can be found in a note file
#[derive(Default)]
pub struct NoteFileData {
    pub date: Option<chrono::NaiveDate>,
    pub tags: Tags,
}

impl NoteFileLinesExtractor for NoteFileData {
    fn handle_line(mut self, line: &str) -> Self {
        self.date = self.date.handle_line(line);
        self.tags = self.tags.handle_line(line);
        self
    }
}

pub trait NoteFileLinesExtractor: Default {
    fn handle_line(self, line: &str) -> Self;
}

lazy_static::lazy_static! {
    static ref DATE_PAT: Regex = Regex::new(r"^yn-date: (\d{4}-\d{2}-\d{2})\s*$").unwrap();
}

impl NoteFileLinesExtractor for Option<chrono::NaiveDate> {
    fn handle_line(self, line: &str) -> Self {
        if self.is_some() {
            return self;
        }

        let date_cap = DATE_PAT.captures(line).and_then(|caps| caps.get(1));

        let captured_date = match date_cap {
            None => return self,
            Some(v) => v.as_str(),
        };

        match captured_date.parse::<chrono::NaiveDate>() {
            Ok(v) => Some(v),
            _ => self,
        }
    }
}

pub struct Tags {
    is_in_tags: bool,
    //todo: convert to set
    tags: Vec<String>,
}

impl Tags {
    pub fn tags(self) -> Vec<String> {
        self.tags
    }
}

impl Default for Tags {
    fn default() -> Self {
        Tags {
            is_in_tags: false,
            tags: Vec::new(),
        }
    }
}

// tag header pat is "tags:" at start of line, followed by any amount of whitespace.
lazy_static::lazy_static! {
    static ref TAG_HEADER_PAT: Regex = Regex::new(r"^tags:\s*$").unwrap();
}

// todo: in this pattern, i'm choosing to take the first "word" on the line as the tag.
// there are several interesting things about this choice:
// * first, it needs to be determined that this is the right choice - as opposed to taking the
//   entire remaining line and trimming.
// * second, if it is the right choice, it needs to be determined that the "word character", as
//   defined [on unicode.org](http://www.unicode.org/reports/tr18/#Compatibility_Properties) is the
//   correct choice. this is not immediately obvious!
// * ideally, the tag-word would correspond with what standard shells (sh, bash, zsh, for example)
//   would consider a word.
// * put differently, if yn is going to be restrictive, yn ought to be *usefully* restrictive.
//
// as for the pattern itself, it expects, in order:
// * 0 or more whitespace at the start of the line
// * the characters `+ `
// * one or more word characters, matched as the tag itself
// * anything goes to the rest of the line
lazy_static::lazy_static! {
    static ref TAG_PAT: Regex = Regex::new(r"^(\s*)\+ (\w+).*$").unwrap();
}

//this imple relies on linear iteration of lines
impl NoteFileLinesExtractor for Tags {
    fn handle_line(mut self, line: &str) -> Self {
        if self.is_in_tags {
            if let Some(captures) = TAG_PAT.captures(line) {
                //todo: do we want to accept indented tags as part of the set, or just drop them?
                //this current setup accepts them
                let word = captures
                    .get(2)
                    .expect("second inner group of regex should always be available")
                    .as_str()
                    .to_owned();

                self.tags.push(word);
            } else {
                self.is_in_tags = false
            }
        } else if TAG_HEADER_PAT.is_match(line) {
            self.is_in_tags = true
        }
        self
    }
}

/// searches a file for the markdown-embedded note metadata that yn defines.
///
/// # Errors
/// this will fail for any sort of IO error. in particular, if the file contains some invalid UTF-8,
/// then this will fail right there.
///
/// todo: figure out if it should return whatever was extracted up to the point of the error.
pub fn read_from_markdown<T, P>(path: P) -> Fallible<T>
where
    T: NoteFileLinesExtractor,
    P: AsRef<Path>,
{
    let f = std::fs::File::open(path.as_ref()).context("could not open note file")?;

    let buf_read = BufReader::new(f);

    buf_read.lines().try_fold(T::default(), |acc, cur| {
        cur.context(format_err!(
            "reading the file {} has failed",
            path.as_ref().display()
        ))
        .map(|l| acc.handle_line(&l))
        .map_err(|e| e.into())
    })
}
