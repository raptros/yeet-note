use std::io::BufWriter;
use std::io::Write;

use failure::Fallible;
use failure::ResultExt;

use tempfile::NamedTempFile;

use super::read_from_markdown;
use super::NoteFileData;
use super::NoteFileLinesExtractor;

fn save_and_scan<T>(bytes: &[u8]) -> Fallible<T>
where
    T: NoteFileLinesExtractor,
{
    let mut tmp = NamedTempFile::new().context("could not open a temp file")?;
    tmp.write_all(bytes)
        .context("failed to write all the bytes to the temp file")?;
    read_from_markdown(tmp)
}

macro_rules! assert_data {
    ($data:expr, $date:expr, $tags: expr) => {
        let tags: Vec<&str> = $tags;
        assert_eq!($data.date, $date);
        assert_eq!($data.tags.tags(), tags);
    };
}

macro_rules! test_markdown_file {
    ($file_name:ident => date $date:expr, tags $tags:expr) => {
        #[test]
        fn $file_name() -> Fallible<()> {
            let bytes =
                include_bytes!(concat!("file-data-samples/", stringify!($file_name), ".md"));
            let data: NoteFileData = save_and_scan(bytes)?;

            assert_data!(data, $date, $tags);

            Ok(())
        }
    };
}

test_markdown_file!(blank => date None, tags Vec::new());

test_markdown_file!(just_lines => date None, tags Vec::new());

test_markdown_file!(malformed_date =>
    date Some(chrono::NaiveDate::from_ymd(2016, 05, 12)),
    tags Vec::new());

test_markdown_file!(simple_date =>
    date Some(chrono::NaiveDate::from_ymd(2018, 12, 31)),
    tags Vec::new());

test_markdown_file!(three_tags =>
    date None,
    tags vec!["one", "two", "three"]);

test_markdown_file!(two_groups_of_tags_and_two_dates =>
    date Some(chrono::NaiveDate::from_ymd(2018, 12, 31)),
    tags vec![
        "one",
        "two",
        "three",
        "twenty30",
        "five_five",
        "pants",
    ]
);

#[test]
fn large_file() -> Fallible<()> {
    let tmp = NamedTempFile::new().context("could not open a temp file")?;

    let mut buf = BufWriter::new(tmp);

    writeln!(
        buf,
        r"
large file test
===============

this is a large file with tags and date somewhere at the middle"
    )
    .context("failed to write header!")?;

    for outer in 0..5 {
        for i in 0..10_000 {
            writeln!(buf, "the current group is {:010} and the current line in that group  is {:>50} and there is a lot going on here.", outer, i)
                .context("junk line")?;
        }
        writeln!(buf, "\n\ntime for tag group {}.\ntags: ", outer).context("tag header")?;
        for i2 in 0..5 {
            writeln!(buf, "+ tag_{}_{}  ", outer, i2).context("tag")?;
        }
        writeln!(buf, "\n").context("newline")?;
    }

    writeln!(buf, "yn-date: 2019-01-02").context("the date")?;

    let tmp = buf
        .into_inner()
        .context("failed to flush buf and get temp file")?;

    let data: NoteFileData =
        read_from_markdown(tmp).context("failed to read the huge temp file!")?;

    let mut expected_tags = Vec::new();

    for outer in 0..5 {
        for inner in 0..5 {
            expected_tags.push(format!("tag_{}_{}", outer, inner).to_string());
        }
    }

    assert_eq!(expected_tags.len(), 25);

    assert_eq!(data.date, Some(chrono::NaiveDate::from_ymd(2019, 01, 02)));
    assert_eq!(data.tags.tags(), expected_tags);

    Ok(())
}

#[test]
fn ten_thousand_tags() -> Fallible<()> {
    let tmp = NamedTempFile::new().context("could not open a temp file")?;

    let mut buf = BufWriter::new(tmp);

    writeln!(
        buf,
        r"
large file test
===============

this file has a lot of tags."
    )
    .context("failed to write header!")?;

    writeln!(buf, "\n\ntime for the tags. \ntags: ").context("tag header")?;

    for i in 0..10_000 {
        writeln!(buf, "+ tag_{}  ", i).context("tag")?;
    }

    writeln!(buf, "yn-date: 2019-01-02").context("the date")?;

    let tmp = buf
        .into_inner()
        .context("failed to flush buf and get temp file")?;

    let data: NoteFileData =
        read_from_markdown(tmp).context("failed to read the huge temp file!")?;

    assert_eq!(data.date, Some(chrono::NaiveDate::from_ymd(2019, 01, 02)));

    let tags = data.tags.tags();

    assert_eq!(tags.len(), 10_000);
    assert_eq!(tags[0], "tag_0");
    assert_eq!(tags[9_999], "tag_9999");

    Ok(())
}
