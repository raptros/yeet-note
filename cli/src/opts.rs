use structopt::StructOpt;
//use structopt::structopt;

#[derive(StructOpt, Debug)]
#[structopt(name = "yn", about = "tool for organizing music")]
pub struct YNOpt {
    #[structopt(subcommand)]
    pub command: YNCommand,
}

#[derive(StructOpt, Debug)]
pub enum YNCommand {
    #[structopt(name = "init")]
    /// Initialize the current directory as the base of a yn "workspace"
    Init,
    #[structopt(name = "update")]
    /// Update the current yn workspace
    Update,
    #[structopt(name = "scandir")]
    /// Exists only for testing purposes.
    //todo: remove
    ScanDir,
}
